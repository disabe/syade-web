# Stage 1: Compile and Build angular codebase

# Use official node image as the base image
FROM node:14.18.3 as build

# Set the working directory
WORKDIR /usr/local/app

# Add the source code to app
COPY . ./

COPY package.json package-lock.json ./
RUN npm install

ENV PATH="./node_modules/.bin:$PATH"

COPY . ./
RUN ng build --prod
# Install all the dependencies
RUN npm install

FROM nginx:1.17-alpine
COPY nginx.config /etc/nginx/conf.d/default.conf
COPY --from=build /usr/local/app/dist/ /usr/share/nginx/html