import { Component, OnInit } from '@angular/core';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/app/dashboard', title: 'Dashboard', icon: 'design_app', class: '' },
  // { path: '/app/icons', title: 'Icons', icon: 'education_atom', class: '' },
  { path: '/app/services', title: 'CRUD Services', icon: 'files_single-copy-04', class: '' },
  //{ path: '/app/notifications', title: 'Notifications', icon: 'ui-1_bell-53', class: '' },
  { path: '/app/clientelle', title: 'Liste des Clients', icon: 'emoticons_satisfied', class: '' },
  { path: '/app/dossiers', title: 'Dossiers Clients', icon: 'files_single-copy-04', class: '' },
  { path: '/app/Employes', title: 'Collaborateurs', icon: 'users_single-02', class: '' },
  //{ path: '/app/user-profile', title: 'User Profile', icon: 'users_single-02', class: '' },
  //{ path: '/app/table-list', title: 'Table List', icon: 'design_bullet-list-67', class: '' },
  //{ path: '/app/typography', title: 'Typography', icon: 'text_caps-small', class: '' }

];

// { path: '/upgrade', title: 'Upgrade to PRO',  icon:'objects_spaceship', class: 'active active-pro' }

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
    if (window.innerWidth > 1650) {
      return false;
    }
    return true;
  };
}
