import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { NotificationService } from '../../config/notification.service';
import { ClienteleModel } from '../clientele';
import { ClienteleApiService } from '../clientele-api.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  constructor(private fb: FormBuilder, private userService: ClienteleApiService, private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService) {
    this.createFactorForm1();

  }


  ngOnInit(): void {
    if (this.route.snapshot.params['id']) {
      this.onUpdate();

    }
  }

  @ViewChild('myClientForm') myClientForm: NgForm;

  public isSaving: boolean = false;
  public clientForm: FormGroup;
  public isCreated: boolean = false;
  private subscriptions: Array<Subscription> = [];
  client: ClienteleModel = null;
  idClient: number;

  onUpdate() {
    this.idClient = this.route.snapshot.params['id'];
    this.userService.getOneClient(this.idClient).subscribe(
      data => {
        // console.log(data);
        this.onGetClient(data);

      },
      err => {
        // console.log(err);
      }
    );

  }

  onGetClient(data: any) {

    this.client = {
      idClient: data.idClient,
      adresseClient: data.adresseClient,
      bp: data.bp,
      dateEnreg: data.dateEnreg,
      emailClient: data.emailClient,
      codeIban: data.codeIban,
      faxClient: data.faxClient,
      matriculeClient: data.matriculeClient,
      minKiloTransport: data.minKiloTransport,
      minMagasin: data.minMagasin,
      minManutention: data.minManutention,
      numeroContribuable: data.numeroContribuable,
      numeroRegCon: data.numeroRegCon,
      raisonSocialeClient: data.raisonSocialeClient,
      siteWebClient: data.siteWebClient,
      tauxADC: data.tauxADC,
      tauxMagasin: data.tauxMagasin,
      tauxManutention: data.tauxManutention,
      tauxTransport: data.tauxTransport,
      telClient: data.telClient,
      typeOperation: data.typeOperation
    }
    console.log('ici client    ', this.client);
    this.createFactorForm();
  }

  createFactorForm1(): void {
    this.clientForm = this.fb.group({

      adresseClient: new FormControl({ value: '', disabled: false }, [
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),

      bp: new FormControl({ value: '', disabled: false }, [
        Validators.minLength(2),
        Validators.maxLength(25)
      ]),
      dateEnreg: new FormControl({ value: new Date(), disabled: false }),
      emailClient: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.email

      ]),
      faxClient: new FormControl({ value: '', disabled: false }),
      matriculeClient: new FormControl({ value: '', disabled: false }),
      minKiloTransport: new FormControl({ value: 0, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      minMagasin: new FormControl({ value: 0, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      minManutention: new FormControl({ value: 0, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      codeIban: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      numeroContribuable: new FormControl({ value: '', disabled: false }),
      numeroRegCon: new FormControl({ value: '', disabled: false }),
      raisonSocialeClient: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      siteWebClient: new FormControl({ value: '', disabled: false }, [
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      tauxADC: new FormControl({ value: 0, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      tauxMagasin: new FormControl({ value: 0, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      tauxManutention: new FormControl({ value: 0, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      tauxTransport: new FormControl({ value: 0, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      telClient: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(14)
      ]),
      typeOperation: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ]),

    });
  }

  createFactorForm(): void {
    this.clientForm = this.fb.group({

      adresseClient: new FormControl({ value: this.client.adresseClient, disabled: false }, [
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),

      bp: new FormControl({ value: this.client.bp, disabled: false }, [
        Validators.minLength(2),
        Validators.maxLength(25)
      ]),
      dateEnreg: new FormControl({ value: this.client.dateEnreg, disabled: false }),
      emailClient: new FormControl({ value: this.client.emailClient, disabled: false }, [
        Validators.required,
        Validators.email

      ]),
      faxClient: new FormControl({ value: this.client.faxClient, disabled: false }),
      matriculeClient: new FormControl({ value: this.client.matriculeClient, disabled: false }),
      minKiloTransport: new FormControl({ value: this.client.minKiloTransport, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      minMagasin: new FormControl({ value: this.client.minMagasin, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      minManutention: new FormControl({ value: this.client.minManutention, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      codeIban: new FormControl({ value: this.client.codeIban, disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      numeroContribuable: new FormControl({ value: this.client.numeroContribuable, disabled: false }),
      numeroRegCon: new FormControl({ value: this.client.numeroRegCon, disabled: false }),
      raisonSocialeClient: new FormControl({ value: this.client.raisonSocialeClient, disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      siteWebClient: new FormControl({ value: this.client.siteWebClient, disabled: false }, [
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      tauxADC: new FormControl({ value: this.client.tauxADC, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      tauxMagasin: new FormControl({ value: this.client.tauxMagasin, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      tauxManutention: new FormControl({ value: this.client.tauxManutention, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      tauxTransport: new FormControl({ value: this.client.tauxTransport, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      telClient: new FormControl({ value: this.client.telClient, disabled: false }, [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(14)
      ]),
      typeOperation: new FormControl({ value: this.client.typeOperation, disabled: false }, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ]),

    });
  }

  /**
    * On form submit
    */
  onSubmit(): void {
    if (this.clientForm.valid) {
      const payload = this.formatData();
      this.updateClient(payload);

    }
  }


  /**
   * Create the client
   * @param payload
   */
  updateClient(payload: any): void {
    this.isSaving = true;
    const saveSub$ = this.userService.updateClient(this.idClient, payload).subscribe(response => {
      if (response.status === 200) {
        this.isCreated = true;
        this.openSnackBar('createClientSuccess');
        this.clientForm.reset();
        //this.enterpriseData = new EnterpriseModel().deserialize(payload);

      } else {
        this.openSnackBar('createClientError');
      }
      this.isSaving = false;
      //this.cdRef.detectChanges();
    }, (error: HttpErrorResponse) => {
      this.isSaving = false;
      this.openSnackBar('createClientError');
      // this.cdRef.detectChanges();
    });
    this.subscriptions.push(saveSub$);
  }



  /**
     * Format data to be saved
     */
  formatData(): any {
    return {
      idClient: this.idClient,
      adresseClient: this.clientForm.get('adresseClient').value,
      bp: this.clientForm.get('bp').value,
      dateEnreg: this.clientForm.get('dateEnreg').value,
      emailClient: this.clientForm.get('emailClient').value,
      faxClient: this.clientForm.get('faxClient').value,
      matriculeClient: this.clientForm.get('matriculeClient').value,
      minKiloTransport: this.clientForm.get('minKiloTransport').value,
      minMagasin: this.clientForm.get('minMagasin').value,
      minManutention: this.clientForm.get('minManutention').value,
      numeroContribuable: this.clientForm.get('numeroContribuable').value,
      numeroRegCon: this.clientForm.get('numeroRegCon').value,
      raisonSocialeClient: this.clientForm.get('raisonSocialeClient').value,
      siteWebClient: this.clientForm.get('siteWebClient').value,
      tauxADC: this.clientForm.get('tauxADC').value,
      codeIban: this.clientForm.get('codeIban').value,
      tauxMagasin: this.clientForm.get('tauxMagasin').value,
      tauxManutention: this.clientForm.get('tauxManutention').value,
      tauxTransport: this.clientForm.get('tauxTransport').value,
      telClient: this.clientForm.get('telClient').value,
      typeOperation: this.clientForm.get('typeOperation').value,
    };
  }


  /**
   * Show snack bar notification based on scenarios
   */
  openSnackBar(section: string): void {
    let notifMsg: string, notifBtn: string;
    switch (section) {

      case 'createClientError':
        notifMsg = 'ERREUR CREATION';
        notifBtn = 'UNE ERREUR S\'EST PRODUITE LORS DE LA MISE A JOUR DU CLIENT';
        this.notificationService.openSnackBar(notifMsg, notifBtn, 'error');
        break;
      case 'createClientSuccess':
        notifMsg = 'SUCCES CREATION ';
        notifBtn = 'CLIENT MIS A JOUR AVEC SUCCES';
        this.notificationService.openSnackBar(notifMsg, notifBtn, 'success');
        break;
      default:
        break;
    }
  }

  /**
   * Fired on destruction
   */
  ngOnDestroy(): void {
    this.unsubscribe(this.subscriptions);
  }

  /**
     * Unsubscriber
     * @param subscriptions Subscription Array or subscription object. Array<Subscription> | Subscription
     */
  unsubscribe(subscriptions: Array<Subscription> | Subscription): void {
    try {
      if (Array.isArray(subscriptions)) {
        subscriptions.forEach(item => {
          item.unsubscribe();
        });
      } else {
        subscriptions.unsubscribe();
      }
    } catch (error) {
    }
  }





}
