import { SelectionModel } from '@angular/cdk/collections';
import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, Subscription } from 'rxjs';
import { NotificationService } from '../config/notification.service';
import { Clientele, ClienteleModel } from './clientele';
import { ClienteleApiService } from './clientele-api.service';
import { ConfirmDialogModel, DeleteComponent } from './delete/delete.component';

@Component({
  selector: 'app-clientelle',
  templateUrl: './clientelle.component.html',
  styleUrls: ['./clientelle.component.css']
})
export class ClientelleComponent implements OnInit {

  constructor(private clientService: ClienteleApiService, private cdRef: ChangeDetectorRef,
    private notificationService: NotificationService,
    public dialog: MatDialog
    //public dialog: MatDialog,
    // private router: Router,
  ) { }


  ngOnInit(): void {
    this.listeClients();
    // this.userService.getAllClient().subscribe(data => this.listeClients = data.data);
    // this.listeClients.forEach(cl => console.log('ici clients  ', cl));
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.clientDataSource.filter = filterValue.trim().toLowerCase();
  }

  private subscriptions: Array<Subscription> = [];
  private activationCode: string;
  public listClients: Array<Clientele> = [];
  public isInitializing: boolean;
  public errorInitializing: boolean;
  clientDataSource = new MatTableDataSource<ClienteleModel>(null);
  clientSelection = new SelectionModel<ClienteleModel>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['nom', 'telephone', 'email', 'operation', 'action']; // 'bp', 'adresse',

  client: Clientele;
  isUpdate: boolean = false;

  listeClients(): void {
    const profileSub$ = this.clientService.getAllClient().subscribe(response => {
      // if (response.status === 200 && response.data) {
      console.log('Data', response);
      this.onGetListeClients(response);
      // } else {
      //   this.isInitializing = false;
      //   this.errorInitializing = true;
      // }
      this.cdRef.detectChanges();
    }, (error: HttpErrorResponse) => {
      this.isInitializing = false;
      this.errorInitializing = true;
      this.cdRef.detectChanges();
    });
    this.subscriptions.push(profileSub$);
  }

  onGetListeClients(data: any) {
    if (Array.isArray(data)) {
      this.listClients = data.map(item => {
        const element: ClienteleModel =
        {
          idClient: item.idClient,
          adresseClient: item.adresseClient,
          bp: item.bp,
          dateEnreg: item.dateEnreg,
          emailClient: item.emailClient,
          faxClient: item.faxClient,
          matriculeClient: item.matriculeClient,
          minKiloTransport: item.minKiloTransport,
          minMagasin: item.minMagasin,
          minManutention: item.minManutention,
          numeroCompteBancaire: item.numeroCompteBancaire,
          numeroContribuable: item.numeroContribuable,
          numeroRegCon: item.numeroRegCon,
          raisonSocialeClient: item.raisonSocialeClient,
          siteWebClient: item.siteWebClient,
          tauxADC: item.tauxADC,
          tauxMagasin: item.tauxMagasin,
          tauxManutention: item.tauxManutention,
          tauxTransport: item.tauxTransport,
          telClient: item.telClient,
          typeOperation: item.typeOperation,
          codeIban: item.codeIban
        };
        return element;
      });
      // console.log('facture List', this.listAvoirs);
    }
    this.clientDataSource = new MatTableDataSource<Clientele>(this.listClients);
    this.clientDataSource.paginator = this.paginator;
    this.isInitializing = false;
    this.errorInitializing = false;
  }

  confirmDialog(ClientID: number): void {

    const message = 'Attention action irreverssible êtes-vous certain de vouloir poursuivre?';

    const dialogData = new ConfirmDialogModel('Confirmation de suppression', message);

    const dialogRef = this.dialog.open(DeleteComponent, {
      maxWidth: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      let result: boolean;
      result = dialogResult;
      if (result) {
        const profileSub$ = this.clientService.deleteClient(ClientID).subscribe(response => {
          console.log('Suppression du client  ', response);
          this.openSnackBar('deleteClientSuccess');
          this.listeClients();
          this.cdRef.detectChanges();
        }, (error: HttpErrorResponse) => {
          this.isInitializing = false;
          this.errorInitializing = true;
          this.openSnackBar('deleteClientError');
          this.cdRef.detectChanges();
        });
        this.subscriptions.push(profileSub$);
      }
    });
  }




  update(cl: Clientele) {
    this.client = cl;
    this.isUpdate = true;
    console.log('ici update  ', this.isUpdate);
  }

  deleteClient(id: number) {
    this.clientService.deleteClient(this.client.idClient);
    console.log('ici delete  ');
  }

  updateCreateClient() {
    if (this.isUpdate === false) {
      this.clientService.addClient(this.client);
      console.log('ici update  ', this.isUpdate);
      console.log('ici add  ', this.client);
    } else {
      this.clientService.updateClient(this.client.idClient, this.client);
      console.log('ici update client  ', this.client);
      console.log('ici update  ', this.isUpdate);
    }
    this.client = null;
    this.isUpdate = false;
  }


  /**
    * Show snack bar notification based on scenarios
    */
  openSnackBar(section: string): void {
    let notifMsg: string, notifBtn: string;
    switch (section) {

      case 'deleteClientError':
        notifMsg = 'ERREUR CREATION';
        notifBtn = 'UNE ERREUR S\'EST PRODUITE LORS DE LA CREATION DU CLIENT';
        this.notificationService.openSnackBar(notifMsg, notifBtn, 'error');
        break;
      case 'deleteClientSuccess':
        notifMsg = 'SUCCES DELETE ';
        notifBtn = 'CLIENT SUPPRIME DE VOTRE LISTE';
        this.notificationService.openSnackBar(notifMsg, notifBtn, 'success');
        break;
      default:
        break;
    }
  }





}

