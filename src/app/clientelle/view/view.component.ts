import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { NotificationService } from '../../config/notification.service';
import { ClienteleModel } from '../clientele';
import { ClienteleApiService } from '../clientele-api.service';



@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  constructor(private fb: FormBuilder, private userService: ClienteleApiService, private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService) {
    this.createFactorForm1();

  }

  ngOnInit(): void {
    if (this.route.snapshot.params['id']) {
      this.onUpdate();

    }
  }

  @ViewChild('myClientForm') myClientForm: NgForm;

  public isSaving: boolean = false;
  public clientForm: FormGroup;
  public isCreated: boolean = false;
  private subscriptions: Array<Subscription> = [];
  client: ClienteleModel = null;
  idClient: number;

  onUpdate() {
    this.idClient = this.route.snapshot.params['id'];
    this.userService.getOneClient(this.idClient).subscribe(
      data => {
        // console.log(data);
        this.onGetClient(data);

      },
      err => {
        // console.log(err);
      }
    );

  }

  onGetClient(data: any) {

    this.client = {
      idClient: data.idClient,
      adresseClient: data.adresseClient,
      bp: data.bp,
      dateEnreg: data.dateEnreg,
      emailClient: data.emailClient,
      codeIban: data.codeIban,
      faxClient: data.faxClient,
      matriculeClient: data.matriculeClient,
      minKiloTransport: data.minKiloTransport,
      minMagasin: data.minMagasin,
      minManutention: data.minManutention,
      numeroCompteBancaire: data.numeroCompteBancaire,
      numeroContribuable: data.numeroContribuable,
      numeroRegCon: data.numeroRegCon,
      raisonSocialeClient: data.raisonSocialeClient,
      siteWebClient: data.siteWebClient,
      tauxADC: data.tauxADC,
      tauxMagasin: data.tauxMagasin,
      tauxManutention: data.tauxManutention,
      tauxTransport: data.tauxTransport,
      telClient: data.telClient,
      typeOperation: data.typeOperation
    }
    console.log('ici client    ', this.client);
    this.createFactorForm();
  }

  createFactorForm1(): void {
    this.clientForm = this.fb.group({

      adresseClient: new FormControl({ value: '', disabled: true }, [
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),

      bp: new FormControl({ value: '', disabled: true }, [
        Validators.minLength(2),
        Validators.maxLength(25)
      ]),
      dateEnreg: new FormControl({ value: new Date(), disabled: true }),
      emailClient: new FormControl({ value: '', disabled: true }, [
        Validators.required,
        Validators.email

      ]),
      faxClient: new FormControl({ value: '', disabled: true }),
      matriculeClient: new FormControl({ value: '', disabled: true }),
      minKiloTransport: new FormControl({ value: 0, disabled: true }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      minMagasin: new FormControl({ value: 0, disabled: true }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      minManutention: new FormControl({ value: 0, disabled: true }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      codeIban: new FormControl({ value: '', disabled: true }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      numeroContribuable: new FormControl({ value: '', disabled: true }),
      numeroRegCon: new FormControl({ value: '', disabled: true }),
      raisonSocialeClient: new FormControl({ value: '', disabled: true }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      siteWebClient: new FormControl({ value: '', disabled: true }, [
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      tauxADC: new FormControl({ value: 0, disabled: true }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      tauxMagasin: new FormControl({ value: 0, disabled: true }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      tauxManutention: new FormControl({ value: 0, disabled: true }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      tauxTransport: new FormControl({ value: 0, disabled: true }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      telClient: new FormControl({ value: '', disabled: true }, [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(14)
      ]),
      typeOperation: new FormControl({ value: '', disabled: true }, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ]),

    });
  }

  createFactorForm(): void {
    this.clientForm = this.fb.group({

      adresseClient: new FormControl({ value: this.client.adresseClient, disabled: true }, [
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),

      bp: new FormControl({ value: this.client.bp, disabled: true }, [
        Validators.minLength(2),
        Validators.maxLength(25)
      ]),
      dateEnreg: new FormControl({ value: this.client.dateEnreg, disabled: true }),
      emailClient: new FormControl({ value: this.client.emailClient, disabled: true }, [
        Validators.required,
        Validators.email

      ]),
      faxClient: new FormControl({ value: this.client.faxClient, disabled: true }),
      matriculeClient: new FormControl({ value: this.client.matriculeClient, disabled: true }),
      minKiloTransport: new FormControl({ value: this.client.minKiloTransport, disabled: true }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      minMagasin: new FormControl({ value: this.client.minMagasin, disabled: true }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      minManutention: new FormControl({ value: this.client.minManutention, disabled: true }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      codeIban: new FormControl({ value: this.client.codeIban, disabled: true }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      numeroContribuable: new FormControl({ value: this.client.numeroContribuable, disabled: true }),
      numeroRegCon: new FormControl({ value: this.client.numeroRegCon, disabled: true }),
      raisonSocialeClient: new FormControl({ value: this.client.raisonSocialeClient, disabled: true }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      siteWebClient: new FormControl({ value: this.client.siteWebClient, disabled: true }, [
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      tauxADC: new FormControl({ value: this.client.tauxADC, disabled: true }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      tauxMagasin: new FormControl({ value: this.client.tauxMagasin, disabled: true }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      tauxManutention: new FormControl({ value: this.client.tauxManutention, disabled: true }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      tauxTransport: new FormControl({ value: this.client.tauxTransport, disabled: true }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      telClient: new FormControl({ value: this.client.telClient, disabled: true }, [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(14)
      ]),
      typeOperation: new FormControl({ value: this.client.typeOperation, disabled: true }, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ]),

    });
  }




  /**
   * Fired on destruction
   */
  ngOnDestroy(): void {
    this.unsubscribe(this.subscriptions);
  }

  /**
     * Unsubscriber
     * @param subscriptions Subscription Array or subscription object. Array<Subscription> | Subscription
     */
  unsubscribe(subscriptions: Array<Subscription> | Subscription): void {
    try {
      if (Array.isArray(subscriptions)) {
        subscriptions.forEach(item => {
          item.unsubscribe();
        });
      } else {
        subscriptions.unsubscribe();
      }
    } catch (error) {
    }
  }

}
