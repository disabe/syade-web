import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Clientele } from './clientele';

const CLIENT_API = 'http://localhost:8888/api/v1/clients';
// // const AUTH_API = 'http://localhost:8080/api/auth/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class ClienteleApiService {


  constructor(private http: HttpClient) { }

  addClient(client: Clientele): Observable<any> {
    console.log('ici service  ', client);
    return this.http.post(CLIENT_API, client, httpOptions);
  }

  getAllClient(): Observable<any> {
    return this.http.get(CLIENT_API + '/all');
  }

  getOneClient(idClient: number): Observable<any> {
    return this.http.get(CLIENT_API + '/client/' + idClient);
  }

  deleteClient(idClient: number): Observable<any> {
    return this.http.delete(CLIENT_API + '/' + idClient);
  }
  updateClient(idClient: number, client: Clientele): Observable<any> {
    return this.http.put(CLIENT_API + '/' + idClient, client);
  }




}
