import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { NotificationService } from '../../config/notification.service';
import { Clientele } from '../clientele';
import { ClienteleApiService } from '../clientele-api.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  constructor(private userService: ClienteleApiService,
    private fb: FormBuilder,
    private cdRef: ChangeDetectorRef,
    private notificationService: NotificationService) {
    this.buildClientForm();
  }


  ngOnInit(): void {

  }

  @ViewChild('myClientForm') myClientForm: NgForm;

  //public isLoading: boolean = true;
  public isSaving: boolean = false;
  public clientForm: FormGroup;
  public isCreated: boolean = false;
  private subscriptions: Array<Subscription> = [];
  listeClients: Observable<Clientele[]>;
  client: Clientele;
  isUpdate = false;



  /**
     * Create enterprise form
     */
  buildClientForm(): void {
    this.clientForm = this.fb.group({

      adresseClient: new FormControl({ value: '', disabled: false }, [

        Validators.minLength(2),
        Validators.maxLength(255)
      ]),

      bp: new FormControl({ value: '', disabled: false }, [

        Validators.minLength(2),
        Validators.maxLength(25)
      ]),
      dateEnreg: new FormControl({ value: new Date(), disabled: false }),
      emailClient: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.email

      ]),
      faxClient: new FormControl({ value: '', disabled: false }),
      matriculeClient: new FormControl({ value: '', disabled: false }),
      minKiloTransport: new FormControl({ value: 0, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      minMagasin: new FormControl({ value: 0, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      minManutention: new FormControl({ value: 0, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      codeIban: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      numeroContribuable: new FormControl({ value: '', disabled: false }),
      numeroRegCon: new FormControl({ value: '', disabled: false }),
      raisonSocialeClient: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      siteWebClient: new FormControl({ value: '', disabled: false }, [
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      tauxADC: new FormControl({ value: 0, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      tauxMagasin: new FormControl({ value: 0, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      tauxManutention: new FormControl({ value: 0, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      tauxTransport: new FormControl({ value: 0, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")
      ]),
      telClient: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(14)
      ]),
      typeOperation: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ]),

    });
  }


  /**
   * On form submit
   */
  onSubmit(): void {
    if (this.clientForm.valid) {
      const payload = this.formatData();
      this.createClient(payload);

    }
  }


  /**
   * Create the client
   * @param payload
   */
  createClient(payload: any): void {
    this.isSaving = true;
    const saveSub$ = this.userService.addClient(payload).subscribe(response => {
      if (response.status === 200) {
        this.isCreated = true;
        this.openSnackBar('createClientSuccess');
        this.clientForm.reset();
        //this.enterpriseData = new EnterpriseModel().deserialize(payload);

      } else {
        this.openSnackBar('createClientError');
      }
      this.isSaving = false;
      this.cdRef.detectChanges();
    }, (error: HttpErrorResponse) => {
      this.isSaving = false;
      this.openSnackBar('createClientError');
      this.cdRef.detectChanges();
    });
    this.subscriptions.push(saveSub$);
  }

  /**
   * Format data to be saved
   */
  formatData(): any {
    return {
      idClient: 0,
      adresseClient: this.clientForm.get('adresseClient').value,
      bp: this.clientForm.get('bp').value,
      dateEnreg: this.clientForm.get('dateEnreg').value,
      emailClient: this.clientForm.get('emailClient').value,
      faxClient: this.clientForm.get('faxClient').value,
      matriculeClient: this.clientForm.get('matriculeClient').value,
      minKiloTransport: this.clientForm.get('minKiloTransport').value,
      minMagasin: this.clientForm.get('minMagasin').value,
      minManutention: this.clientForm.get('minManutention').value,
      numeroContribuable: this.clientForm.get('numeroContribuable').value,
      numeroRegCon: this.clientForm.get('numeroRegCon').value,
      raisonSocialeClient: this.clientForm.get('raisonSocialeClient').value,
      siteWebClient: this.clientForm.get('siteWebClient').value,
      tauxADC: this.clientForm.get('tauxADC').value,
      codeIban: this.clientForm.get('codeIban').value,
      tauxMagasin: this.clientForm.get('tauxMagasin').value,
      tauxManutention: this.clientForm.get('tauxManutention').value,
      tauxTransport: this.clientForm.get('tauxTransport').value,
      telClient: this.clientForm.get('telClient').value,
      typeOperation: this.clientForm.get('typeOperation').value,
    };
  }


  /**
   * Show snack bar notification based on scenarios
   */
  openSnackBar(section: string): void {
    let notifMsg: string, notifBtn: string;
    switch (section) {

      case 'createClientError':
        notifMsg = 'ERREUR CREATION';
        notifBtn = 'UNE ERREUR S\'EST PRODUITE LORS DE LA CREATION DU CLIENT';
        this.notificationService.openSnackBar(notifMsg, notifBtn, 'error');
        break;
      case 'createClientSuccess':
        notifMsg = 'SUCCES CREATION ';
        notifBtn = 'CLIENT ENREGISTRE AVEC SUCCES';
        this.notificationService.openSnackBar(notifMsg, notifBtn, 'success');
        break;
      default:
        break;
    }
  }

  /**
   * Fired on destruction
   */
  ngOnDestroy(): void {
    this.unsubscribe(this.subscriptions);
  }

  /**
     * Unsubscriber
     * @param subscriptions Subscription Array or subscription object. Array<Subscription> | Subscription
     */
  unsubscribe(subscriptions: Array<Subscription> | Subscription): void {
    try {
      if (Array.isArray(subscriptions)) {
        subscriptions.forEach(item => {
          item.unsubscribe();
        });
      } else {
        subscriptions.unsubscribe();
      }
    } catch (error) {
    }
  }


  update(cl: Clientele) {
    this.client = cl;
    this.isUpdate = true;
    console.log('ici update  ', this.isUpdate);
  }

  deleteClient(id: number) {
    this.userService.deleteClient(this.client.idClient);
    console.log('ici delete  ');
  }

  updateCreateClient() {
    if (this.isUpdate === false) {
      this.userService.addClient(this.client);
      console.log('ici update  ', this.isUpdate);
      console.log('ici add  ', this.client);
    } else {
      this.userService.updateClient(this.client.idClient, this.client);
      console.log('ici update client  ', this.client);
      console.log('ici update  ', this.isUpdate);
    }
    this.client;
    this.isUpdate = false;
  }



}
