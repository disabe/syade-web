export class Clientele {

  constructor(
    dateEnreg: Date,
    emailClient: string,
    faxClient: string,
    matriculeClient: string,
    minKiloTransport: number,
    minMagasin: number,
    minManutention: number,
    raisonSocialeClient: string,
    tauxADC: number,
    tauxMagasin: number,
    tauxManutention: number,
    tauxTransport: number,
    telClient: string,
    typeOperation: string,
    idClient?: number,
    adresseClient?: string,
    siteWebClient?: string,
    numeroCompteBancaire?: string,
    numeroContribuable?: string,
    numeroRegCon?: string,
    bp?: string,
  ) {
    this.idClient = idClient
    this.adresseClient = adresseClient;
    this.bp = bp;
    this.dateEnreg = dateEnreg;
    this.emailClient = emailClient;
    this.faxClient = faxClient;
    this.matriculeClient = matriculeClient;
    this.minKiloTransport = minKiloTransport;
    this.minMagasin = minMagasin;
    this.minManutention = minManutention;
    this.numeroCompteBancaire = numeroCompteBancaire;
    this.numeroContribuable = numeroContribuable;
    this.numeroRegCon = numeroRegCon;
    this.raisonSocialeClient = raisonSocialeClient;
    this.siteWebClient = siteWebClient;
    this.tauxADC = tauxADC;
    this.tauxMagasin = tauxMagasin;
    this.tauxManutention = tauxManutention;
    this.tauxTransport = tauxTransport;
    this.telClient = telClient;
    this.typeOperation = typeOperation;
  }

  idClient?: number = 0;
  adresseClient?: string;
  bp?: string;
  dateEnreg: Date;
  emailClient: string;
  codeIban: string;
  faxClient: string;
  matriculeClient: string;
  minKiloTransport: number;
  minMagasin: number;
  minManutention: number;
  numeroCompteBancaire?: string;
  numeroContribuable?: string;
  numeroRegCon?: string;
  raisonSocialeClient: string;
  siteWebClient?: string;
  tauxADC: number;
  tauxMagasin: number;
  tauxManutention: number;
  tauxTransport: number;
  telClient: string;
  typeOperation: string;
}

export interface ClienteleModel {
  idClient?: number;
  adresseClient?: string;
  bp?: string;
  dateEnreg: Date;
  emailClient: string;
  codeIban: string;
  faxClient: string;
  matriculeClient: string;
  minKiloTransport: number;
  minMagasin: number;
  minManutention: number;
  numeroCompteBancaire?: string;
  numeroContribuable?: string;
  numeroRegCon?: string;
  raisonSocialeClient: string;
  siteWebClient?: string;
  tauxADC: number;
  tauxMagasin: number;
  tauxManutention: number;
  tauxTransport: number;
  telClient: string;
  typeOperation: string;
}
