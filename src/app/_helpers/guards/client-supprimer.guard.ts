import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { role } from '../../config/role';
import { TokenStorageService } from '../../_services/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class ClientSupprimerGuard implements CanActivate {
  constructor(private token: TokenStorageService) {
    this.myuser = token.getUser();
    this.listRole = this.myuser.roles;
  }
  myuser: any;
  listRole: string[] = [];
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    let existe = false;

    const it: string = `${role.clientSupprimer.libelle_role}`;
    this.listRole.find(element => {
      if (element === it) {
        existe = true;
      }
    });
    if (existe) {
      return true;
    }
    else {
      alert('Vous ne pouvez acceder à cette ressource');
      existe = false;
    }


  }


}
