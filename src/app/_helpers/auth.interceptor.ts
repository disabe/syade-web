import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';

import { TokenStorageService } from '../_services/token-storage.service';
import { Observable } from 'rxjs';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private token: TokenStorageService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authToken = this.token.getToken();
    req = req.clone({
      setHeaders: {
        'Access-Control-Allow-Origin': "*",
        'Access-Control-Allow-Methods': "GET , PUT , POST , DELETE",
        'Access-Control-Allow-Headers': "Content-Type, x-requested-with",
        Authorization: "Bearer " + authToken
      }
    });
    console.log('ici req  ', req);
    return next.handle(req);
  }
}

//export const authInterceptorProviders = [
//  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
//];