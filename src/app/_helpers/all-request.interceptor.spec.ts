import { TestBed } from '@angular/core/testing';

import { AllRequestInterceptor } from './all-request.interceptor';

describe('AllRequestInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      AllRequestInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: AllRequestInterceptor = TestBed.inject(AllRequestInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
