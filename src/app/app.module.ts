import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';


import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSnackBarModule } from '@angular/material/snack-bar/';
import { MatSliderModule } from '@angular/material/slider';
import { MatChipsModule } from '@angular/material/chips';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ChartsModule } from 'ng2-charts';
import { DossiersComponent } from './dossiers/dossiers.component';
import { OuvertureComponent } from './dossiers/ouverture/ouverture.component';
import { ModificationComponent } from './dossiers/modification/modification.component';
import { VisualisationComponent } from './dossiers/visualisation/visualisation.component';
import { EmployeesComponent } from './employees/employees.component';
import { CreateEmpComponent } from './employees/create-emp/create-emp.component';
import { UpdateEmpComponent } from './employees/update-emp/update-emp.component';
import { DeleteEmpComponent } from './employees/delete-emp/delete-emp.component';
import { LoginComponent } from './user-profile/login/login.component';
import { DossierManagementComponent } from './dossier-management/dossier-management.component';
import { CodifierComponent } from './dossiers/visualisation/codifier/codifier.component';
import { NoCommaPipe } from './shared-core/no-comma.pipe';
import { ServiceCrudComponent } from './dossiers/service-crud/service-crud.component';
import { PilotageComponent } from './dossiers/pilotage/pilotage.component';
import { FinalisationDossierComponent } from './dossiers/finalisation-dossier/finalisation-dossier.component';
import { AuthInterceptor } from './_helpers/auth.interceptor';
@NgModule({
  imports: [
    HttpClientModule,
    //AuthInterceptor,
    BrowserAnimationsModule,
    FormsModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    NgbModule,
    MatSliderModule,
    MatTableModule,
    MatChipsModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    ChartsModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatMenuModule,
    MatDialogModule,
    MatButtonToggleModule,
    MatListModule,
    MatExpansionModule,
    MatTooltipModule,
    MatTabsModule,
    ToastrModule.forRoot()
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    DossiersComponent,
    OuvertureComponent,
    ModificationComponent,
    VisualisationComponent,
    EmployeesComponent,
    CreateEmpComponent,
    UpdateEmpComponent,
    DeleteEmpComponent,
    LoginComponent,
    DossierManagementComponent,
    CodifierComponent,
    NoCommaPipe,
    ServiceCrudComponent,
    PilotageComponent,
    FinalisationDossierComponent

  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
