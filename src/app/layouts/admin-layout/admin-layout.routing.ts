import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { ClientelleComponent } from '../../clientelle/clientelle.component';
import { UpdateComponent } from '../../clientelle/update/update.component';
import { CreateComponent } from '../../clientelle/create/create.component';
import { ViewComponent } from '../../clientelle/view/view.component';
import { DossiersComponent } from '../../dossiers/dossiers.component';
import { OuvertureComponent } from '../../dossiers/ouverture/ouverture.component';
import { VisualisationComponent } from '../../dossiers/visualisation/visualisation.component';
import { ModificationComponent } from '../../dossiers/modification/modification.component';
import { EmployeesComponent } from '../../employees/employees.component';
import { CreateEmpComponent } from '../../employees/create-emp/create-emp.component';
import { UpdateEmpComponent } from '../../employees/update-emp/update-emp.component';
import { DossierManagementComponent } from '../../dossier-management/dossier-management.component';
import { DossierListGuard } from '../../_helpers/guards/dossier-list.guard';
import { DossierCreerGuard } from '../../_helpers/guards/dossier-creer.guard';
import { DossierVisualiserGuard } from '../../_helpers/guards/dossier-visualiser.guard';
import { DossierAttribuerGuard } from '../../_helpers/guards/dossier-attribuer.guard';
import { EmployerGererGuard } from '../../_helpers/guards/employer-gerer.guard';
import { ClientGererGuard } from '../../_helpers/guards/client-gerer.guard';
import { ServiceCrudComponent } from '../../dossiers/service-crud/service-crud.component';
import { PilotageComponent } from '../../dossiers/pilotage/pilotage.component';
import { FinalisationDossierComponent } from '../../dossiers/finalisation-dossier/finalisation-dossier.component';

export const AdminLayoutRoutes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'user-profile', component: UserProfileComponent },
  { path: 'table-list', component: TableListComponent },
  { path: 'typography', component: TypographyComponent },
  { path: 'icons', component: IconsComponent },
  { path: 'maps', component: MapsComponent },
  { path: 'notifications', component: NotificationsComponent },
  { path: 'clientelle', canActivate: [ClientGererGuard], component: ClientelleComponent },
  { path: 'client/UpdateClient/:id', canActivate: [ClientGererGuard], component: UpdateComponent },
  { path: 'client/ViewClient/:id', canActivate: [ClientGererGuard], component: ViewComponent },
  { path: 'NewClient', canActivate: [ClientGererGuard], component: CreateComponent },
  { path: 'dossiers', canActivate: [DossierCreerGuard], component: DossiersComponent },
  { path: 'services', canActivate: [DossierCreerGuard], component: ServiceCrudComponent },
  { path: 'pilotage/:idDossier', canActivate: [DossierCreerGuard], component: PilotageComponent },
  { path: 'finalisation/:idDossier', canActivate: [DossierCreerGuard], component: FinalisationDossierComponent },
  { path: 'dossier/ouverture/:idClient', canActivate: [DossierCreerGuard], component: OuvertureComponent },
  { path: 'dossier/visualiser/:idDossier', canActivate: [DossierVisualiserGuard], component: VisualisationComponent },
  { path: 'dossier/modifier/:idDossier', canActivate: [DossierCreerGuard], component: ModificationComponent },
  { path: 'Employes', canActivate: [EmployerGererGuard], component: EmployeesComponent },
  { path: 'NewEmployee', canActivate: [EmployerGererGuard], component: CreateEmpComponent },
  { path: 'dossier/GererDossier', canActivate: [DossierAttribuerGuard], component: DossierManagementComponent },
  { path: 'modifierEmployee/:idEmp', canActivate: [EmployerGererGuard], component: UpdateEmpComponent },
  { path: 'upgrade', component: UpgradeComponent }

];
