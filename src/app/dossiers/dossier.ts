export interface DossierModel {
  idDossier?: number;

  cde?: string;

  cne?: string;

  da?: string;

  dateBC?: Date;

  datecloture?: Date;

  statut?: string;

  detailsBC?: string;

  dossier?: string;

  lta?: number;

  montantEXO?: number;

  montantFac?: number;

  montantHTBonC?: number;

  montantIR?: number;

  montantTaxale?: number;

  montantTSR?: number;

  montantTTCBonC?: number;

  montantTVA?: number;

  netAPayerBonC?: number;

  numeroBC?: string;

  objetBC?: string;

  ot?: string;

  poids?: number;

  proformaOK?: string;

  tauxChange?: number;

  tauxTSR?: number;

  tvaBC?: number;

  typeFacture?: string;
  clientele?: number;
  valCipCfa?: number;
	valFobCfa?: number;
	valFob?: number;
	montantCip?: number;
	montantAssurance?: number;
	montantTransport?: number;
  declarant?: number;
  nomDeclarant?: string;
  numFactureFournisseur?: string;
  declarationImportation?: string;
  navireVol?: string;
  numFactureFret?: string;
  nbrColis?: number;
  numAssurance?: string;
  numCertificatSgs?: string;
  certificatOrigine?: string;
  certificatDispense?: string;
  certificatPhytosanitaire?: string;
  adresseLivraison?: string;
  autreDocuments?: string;
}

export interface DeviseModel {
  idDevise?: number;

  libelle?: string;

  valeurCfa?: number;
}
export interface ServiceModel {

  idService?: number;

  categorie?: string;

  libelleService?: string;

  prixMax?: number;


}

export interface LigneFacturationModel {
  idLignCMD?: number;

  dateLivCMD?: Date;

  montantHT?: number;

  montantTVA?: number;

  prixTotal?: number;
  tauxTVA?: number;

  tvaExterne?: number;
  prixUnitaire: number;

  idDossier: number;

  idService: number;
  libelleService?: string;
  categorie?: string;


}

