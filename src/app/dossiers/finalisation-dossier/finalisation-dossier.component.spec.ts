import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalisationDossierComponent } from './finalisation-dossier.component';

describe('FinalisationDossierComponent', () => {
  let component: FinalisationDossierComponent;
  let fixture: ComponentFixture<FinalisationDossierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinalisationDossierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalisationDossierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
