import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { DeviseModel, DossierModel, LigneFacturationModel, ServiceModel } from "./dossier";

const DOSSIER_API = "http://localhost:8888/api/v1/dossiers";
const DEVISE_API = "http://localhost:8888/api/v1/devises";
const SERVICE_API = "http://localhost:8888/api/v1/services";
const FACTURATION_API = "http://localhost:8888/api/v1/facturation";
// // const AUTH_API = 'http://localhost:8080/api/auth/';

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" }),
};

@Injectable({
  providedIn: "root",
})
export class DossiersApiService {
  constructor(private http: HttpClient) { }

  findFacturesPdf(idDossier: number) {
    const httpOptions = {
      responseType: 'arraybuffer' as 'json'
      // 'responseType'  : 'blob' as 'json'        //This also worked
    };
    return this.http.get(FACTURATION_API + "/facture/pdf/" + idDossier, httpOptions);
  }


  facturerDossier(idDossier: number) {
    return this.http.get(FACTURATION_API + "/faturer/" + idDossier);
  }
  // lister les servises d'un dossier
  getAllServicesDossier(idDossier: number): Observable<any> {
    return this.http.get(FACTURATION_API + "/servicesDossier/" + idDossier);
  }

  deleteServiceOnDossier(idLigne: number): Observable<any> {
    return this.http.delete(FACTURATION_API + "/" + idLigne);
  }


  //Opérations sur la facturation
  addServiceOnDossier(liste: Array<LigneFacturationModel>): Observable<any> {
    return this.http.post(FACTURATION_API, liste, httpOptions);
  }


  //Opérations sur les services
  addService(service: ServiceModel): Observable<any> {
    return this.http.post(SERVICE_API, service, httpOptions);
  }

  getAllServices(): Observable<any> {
    return this.http.get(SERVICE_API + "/all");
  }

  updateService(idService: number, service: ServiceModel): Observable<any> {
    return this.http.put(SERVICE_API + "/" + idService, service);
  }
  deleteService(idService: number): Observable<any> {
    return this.http.delete(SERVICE_API + "/" + idService);
  }

  //Opérations sur les dévises
  addDevise(devise: DeviseModel): Observable<any> {
    return this.http.post(DEVISE_API, devise, httpOptions);
  }

  getAllDevises(): Observable<any> {
    return this.http.get(DEVISE_API + "/all");
  }

  updateDevise(idDevise: number, devise: DeviseModel): Observable<any> {
    return this.http.put(DEVISE_API + "/" + idDevise, devise);
  }
  deleteDevise(idDevise: number): Observable<any> {
    return this.http.delete(DEVISE_API + "/" + idDevise);
  }

  //Opérations sur les dossiers
  addDossier(dossier: DossierModel): Observable<any> {
    return this.http.post(DOSSIER_API, dossier, httpOptions);
  }

  addDossierToDeclarant(
    listeDossier: Array<number>,
    idDeclarant: number
  ): Observable<any> {
    return this.http.post(
      DOSSIER_API + "/attribuerDossier/" + idDeclarant,
      listeDossier,
      httpOptions
    );
  }

  getAllDossiers(): Observable<any> {
    return this.http.get(DOSSIER_API + "/all");
  }
  getDossiersLibres(): Observable<any> {
    return this.http.get(DOSSIER_API + "/dossierLibre");
  }

  getDossiersByClient(idClient: number): Observable<any> {
    return this.http.get(DOSSIER_API + "/client/" + idClient);
  }
  getDossiersByDeclarant(idDeclarant: number): Observable<any> {
    return this.http.get(DOSSIER_API + "/dossierDeclarant/" + idDeclarant);
  }

  getOneDossier(idDossier: number): Observable<any> {
    return this.http.get(DOSSIER_API + "/dossier/" + idDossier);
  }

  deleteDossier(idDossier: number): Observable<any> {
    return this.http.delete(DOSSIER_API + "/" + idDossier);
  }

  updateDossier(idDossier: number, dossier: DossierModel): Observable<any> {
    return this.http.put(DOSSIER_API + "/" + idDossier, dossier);
  }

  getAllCode(): Observable<any> {
    return this.http.get(DOSSIER_API + "/listeSH/allCode");
  }

  getSHBYCode(code: string): Observable<any> {
    return this.http.get(DOSSIER_API + "/listeSH/code/" + code);
  }

  getAllLibelle(): Observable<any> {
    return this.http.get(DOSSIER_API + "/listeSH/allLibelle");
  }

  getSHBYLibelle(lib: string): Observable<any> {
    return this.http.get(DOSSIER_API + "/listeSH/libelle/" + lib);
  }

  saveMachandisesDossier(
    idDossier: number,
    marchandises: any
  ): Observable<any> {
    return this.http.post(DOSSIER_API + "/codifier/" + idDossier, marchandises);
  }

  getMarchandiseDossier(idDossier: number): Observable<any> {
    return this.http.get(DOSSIER_API + "/marchandises/" + idDossier);
  }

  deleteMarchandiseDossier(idMarchandise: number): Observable<any> {
    return this.http.delete(
      DOSSIER_API + "/marchandises/delete/" + idMarchandise
    );
  }
}
