import { HttpErrorResponse } from "@angular/common/http";
import { Component, OnInit, ViewChild } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  NgForm,
  Validators,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { NotificationService } from "../../config/notification.service";
import { DossierModel, DeviseModel } from "../dossier";
import { DossiersApiService } from "../dossiers-api.service";

@Component({
  selector: "app-ouverture",
  templateUrl: "./ouverture.component.html",
  styleUrls: ["./ouverture.component.css"],
})
export class OuvertureComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private dossierService: DossiersApiService,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    if (this.route.snapshot.params["idClient"]) {
      this.idClient = this.route.snapshot.params["idClient"];
      this.builddossierForm();
      this.getDevises();
    }
  }

  @ViewChild("mydossierForm") mydossierForm: NgForm;

  public isSaving: boolean = false;
  public dossierForm: FormGroup;
  public isCreated: boolean = false;
  private subscriptions: Array<Subscription> = [];
  dossier: DossierModel = null;
  idClient: number;
  listeDevises: Array<DeviseModel> = [];

  getDevises() {
    const codeSub$ = this.dossierService.getAllDevises().subscribe(
      (response) => {
        this.onGetDevises(response);
      },
      (error: HttpErrorResponse) => {}
    );
    this.subscriptions.push(codeSub$);
  }

  onGetDevises(data: any[]) {
    if (Array.isArray(data)) {
      this.listeDevises = data.map((item) => {
        const element: DeviseModel = {
          libelle: item.libelle,
          idDevise: item.idDevise,
          valeurCfa: item.valeurCfa,
        };
        return element;
      });
    }
  }

  /**
   * Create enterprise form
   */
  builddossierForm(): void {
    this.dossierForm = this.fb.group({
      clientele: new FormControl({ value: this.idClient }),
      cde: new FormControl({ value: "", disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
      valFob: new FormControl({ value: 0, disabled: false }, [
        //Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$"),
      ]),
      montantCip: new FormControl({ value: 0, disabled: false }, [
        //Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$"),
      ]),
      montantAssurance: new FormControl({ value: 0, disabled: false }, [
        //Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$"),
      ]),
      montantTransport: new FormControl({ value: 0, disabled: false }, [
        //Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$"),
      ]),
      tauxChange: new FormControl({ value: 655.95, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,3})?$"),
      ]),
      poids: new FormControl({ value: 0, disabled: false }, [
        Validators.required,
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$"),
      ]),
      ot: new FormControl({ value: "", disabled: false }),
      objetBC: new FormControl({ value: "", disabled: false }),
      lta: new FormControl({ value: "", disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255),
      ]),
      dossier: new FormControl({ value: "", disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255),
      ]),
      statut: new FormControl({ value: "CREATION", disabled: false }),
      dateBC: new FormControl({ value: new Date(), disabled: false }, []),
      da: new FormControl({ value: "", disabled: false }, [
        Validators.minLength(2),
        Validators.maxLength(255),
      ]),

      cne: new FormControl({ value: "", disabled: false }, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      numFactureFournisseur: new FormControl({ value: "", disabled: false }, [
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      declarationImportation: new FormControl({ value: "", disabled: false }, [
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      navireVol: new FormControl({ value: "", disabled: false }, [
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      numFactureFret: new FormControl({ value: "", disabled: false }, [
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      nbrColis: new FormControl({ value: 1, disabled: false }, [
        Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$"),
      ]),
      numAssurance: new FormControl({ value: "", disabled: false }, [
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      numCertificatSgs: new FormControl({ value: "", disabled: false }, [
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      certificatOrigine: new FormControl({ value: "", disabled: false }, [
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      certificatDispense: new FormControl({ value: "", disabled: false }, [
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      certificatPhytosanitaire: new FormControl(
        { value: "", disabled: false },
        [Validators.minLength(1), Validators.maxLength(255)]
      ),
      adresseLivraison: new FormControl({ value: "", disabled: false }, [
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      autreDocuments: new FormControl({ value: "", disabled: false }, [
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
    });
  }

  /**
   * On form submit
   */
  onSubmit(): void {
    if (this.dossierForm.valid) {
      const payload = this.formatData();
      this.createDossier(payload);
    }
  }

  /**
   * Create the Dossier
   * @param payload
   */
  createDossier(payload: any): void {
    this.isSaving = true;
    const saveSub$ = this.dossierService.addDossier(payload).subscribe(
      (response) => {
        if (response) {
          this.isCreated = true;
          this.openSnackBar("createClientSuccess");
          this.dossierForm.reset();
          //this.enterpriseData = new EnterpriseModel().deserialize(payload);
        } else {
          this.openSnackBar("createClientError");
        }
        this.isSaving = false;
        // this.cdRef.detectChanges();
      },
      (error: HttpErrorResponse) => {
        this.isSaving = false;
        this.openSnackBar("createClientError");
        // this.cdRef.detectChanges();
      }
    );
    this.subscriptions.push(saveSub$);
  }

  /**
   * Format data to be saved
   */
  formatData(): any {
    return {
      clientele: this.idClient,
      cde: this.dossierForm.get("cde").value,
      //valCipCfa: this.dossierForm.get("valCipCfa").value,
      //valFobCfa: this.dossierForm.get("valFobCfa").value,
      valFob: this.dossierForm.get("valFob").value,
      montantCip: this.dossierForm.get("montantCip").value,
      montantAssurance: this.dossierForm.get("montantAssurance").value,
      montantTransport: this.dossierForm.get("montantTransport").value,
      tauxChange: this.dossierForm.get("tauxChange").value,
      poids: this.dossierForm.get("poids").value,
      ot: this.dossierForm.get("ot").value,

      lta: this.dossierForm.get("lta").value,

      dossier: this.dossierForm.get("dossier").value,

      statut: this.dossierForm.get("statut").value,
      da: this.dossierForm.get("da").value,

      dateBC: this.dossierForm.get("dateBC").value,

      cne: this.dossierForm.get("cne").value,

      numFactureFournisseur: this.dossierForm.get("numFactureFournisseur")
        .value,
      declarationImportation: this.dossierForm.get("declarationImportation")
        .value,
      navireVol: this.dossierForm.get("navireVol").value,
      numFactureFret: this.dossierForm.get("numFactureFret").value,
      nbrColis: this.dossierForm.get("nbrColis").value,
      numAssurance: this.dossierForm.get("numAssurance").value,
      numCertificatSgs: this.dossierForm.get("numCertificatSgs").value,
      certificatOrigine: this.dossierForm.get("certificatOrigine").value,
      certificatDispense: this.dossierForm.get("certificatDispense").value,
      certificatPhytosanitaire: this.dossierForm.get("certificatPhytosanitaire")
        .value,
      adresseLivraison: this.dossierForm.get("adresseLivraison").value,
      autreDocuments: this.dossierForm.get("autreDocuments").value
    };
  }

  /**
   * Show snack bar notification based on scenarios
   */
  openSnackBar(section: string): void {
    let notifMsg: string, notifBtn: string;
    switch (section) {
      case "createClientError":
        notifMsg = "ERREUR CREATION";
        notifBtn = "UNE ERREUR S'EST PRODUITE LORS DE LA CREATION DU CLIENT";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "error");
        break;
      case "createClientSuccess":
        notifMsg = "SUCCES CREATION ";
        notifBtn = "CLIENT ENREGISTRE AVEC SUCCES";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "success");
        break;
      default:
        break;
    }
  }

  /**
   * Fired on destruction
   */
  ngOnDestroy(): void {
    this.unsubscribe(this.subscriptions);
  }

  /**
   * Unsubscriber
   * @param subscriptions Subscription Array or subscription object. Array<Subscription> | Subscription
   */
  unsubscribe(subscriptions: Array<Subscription> | Subscription): void {
    try {
      if (Array.isArray(subscriptions)) {
        subscriptions.forEach((item) => {
          item.unsubscribe();
        });
      } else {
        subscriptions.unsubscribe();
      }
    } catch (error) {}
  }
}
