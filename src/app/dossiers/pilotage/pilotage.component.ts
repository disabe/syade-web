import { SelectionModel } from '@angular/cdk/collections';
import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { NotificationService } from '../../config/notification.service';
import { DossierModel, LigneFacturationModel, ServiceModel } from '../dossier';
import { DossiersApiService } from '../dossiers-api.service';
import { ConfirmDialogModel, DeleteComponent } from '../../clientelle/delete/delete.component';

@Component({
  selector: 'app-pilotage',
  templateUrl: './pilotage.component.html',
  styleUrls: ['./pilotage.component.css']
})
export class PilotageComponent implements OnInit {
  idDossier: number;

  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private dossierService: DossiersApiService,
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.params["idDossier"]) {
      this.idDossier = this.route.snapshot.params["idDossier"];
      this.getDossier();
      this.listeServicesDossier();
    }
    this.listeServices();
  }

  dossier: DossierModel = null;
  public isSaving: boolean = false;
  public isEditing: boolean = false;
  public serviceForm: FormGroup;
  public isCreated: boolean = false;
  private subscriptions: Array<Subscription> = [];
  serviceDebour: ServiceModel = null;
  servicePrestation: ServiceModel = null;
  serviceDeboursDataSource = new MatTableDataSource<LigneFacturationModel>(null);
  servicePrestationsDataSource = new MatTableDataSource<LigneFacturationModel>(null);
  serviceSelection = new SelectionModel<ServiceModel>(true, []);
  public listServicesDebours: Array<ServiceModel> = [];
  public listServicesPrestations: Array<ServiceModel> = [];
  public ligneFacturationDebours: LigneFacturationModel = null;
  public ligneFacturationPrestations: LigneFacturationModel = null;
  public listeLignePrestations: Array<LigneFacturationModel> = [];
  public listeLigne: Array<LigneFacturationModel> = [];
  public listeLigneDebours: Array<LigneFacturationModel> = [];
  public listeNewLigneDebours: Array<LigneFacturationModel> = [];
  @ViewChild('prestationsPaginator', { static: true }) prestationsPaginator: MatPaginator;
  @ViewChild('deboursPaginator', { static: true }) deboursPaginator: MatPaginator;
  displayedColumns: string[] = ['libelle', 'cout', 'action'];
  isCreate: boolean = false;
  prixDebours: number = 0;
  prixPrestations: number = 0;
  libPrestation: string = '';
  libDebours: string = '';
  isNewDebours: boolean = false;
  isNewPrestation: boolean = false;

  onUpdate(event) {
    if (event.categorie === 'Debours') {

    }
    else {

    }
  }




  confirmDialogSer(event): void {
    const message =
      "Attention action irreverssible êtes-vous certain de vouloir poursuivre?";

    const dialogData = new ConfirmDialogModel(
      "Confirmation de suppression",
      message
    );

    const dialogRef = this.dialog.open(DeleteComponent, {
      maxWidth: "400px",
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      let liste: Array<LigneFacturationModel> = [];

      let result: boolean;
      result = dialogResult;
      if (result) {
        if (this.findLibelleExist(event.libelleService, this.listeNewLigneDebours)) {
          console.log('Data event', event);
          if (event.categorie === 'Debours') {
            console.log('Data debours');
            const findDosIndex = this.listeLigneDebours.findIndex(
              (cate) => cate === event
            );
            if (findDosIndex !== -1) {
              this.listeLigneDebours.splice(findDosIndex, 1);
              console.log('Data debours', this.listeLigneDebours);
              //this.cdRef.detectChanges();
            }

          }
          else {
            console.log('Data presta 1');
            const findDosIndex = this.listeLignePrestations.findIndex(
              (cate) => cate === event
            );
            if (findDosIndex !== -1) {
              this.listeLignePrestations.splice(findDosIndex, 1);
              console.log('Data presta', this.listeLignePrestations);
            }

            //this.cdRef.detectChanges();
          }

        } else {
          const libSub$ = this.dossierService
            .deleteServiceOnDossier(event.idLignCMD)
            .subscribe(
              (response) => {
                if (event.categorie === 'Debours') {
                  console.log('Data debours');
                  const findDosIndex = this.listeLigneDebours.findIndex(
                    (cate) => cate === event
                  );
                  if (findDosIndex !== -1) {
                    this.listeLigneDebours.splice(findDosIndex, 1);
                    console.log('Data debours', this.listeLigneDebours);
                    //this.cdRef.detectChanges();
                  }

                }
                else {
                  console.log('Data presta 1');
                  const findDosIndex = this.listeLignePrestations.findIndex(
                    (cate) => cate === event
                  );
                  if (findDosIndex !== -1) {
                    this.listeLignePrestations.splice(findDosIndex, 1);
                    console.log('Data presta', this.listeLignePrestations);
                  }

                  //this.cdRef.detectChanges();
                }
                this.openSnackBar("deleteServiceSuccess");
              },
              (error: HttpErrorResponse) => {
                this.openSnackBar("deleteServiceError");
              }
            );
          this.subscriptions.push(libSub$);
          this.cdRef.detectChanges();
        }
        liste = [].concat(this.listeLigneDebours, this.listeLignePrestations);
        console.log('Data liste', liste);
        this.onGetListeServicesDossier(liste);
      }
    });
  }

  listeServicesDossier(): void {

    const profileSub$ = this.dossierService.getAllServicesDossier(this.idDossier).subscribe(response => {
      // if (response.status === 200 && response.data) {
      console.log('Data serv dossier ', response);
      this.onGetListeServicesDossier(response);

    }, (error: HttpErrorResponse) => {

    });
    this.subscriptions.push(profileSub$);
  }

  onGetListeServicesDossier(data: any) {
    this.listeLigneDebours = [];
    this.listeLignePrestations = [];
    // this.serviceDeboursDataSource = new MatTableDataSource<null>();
    // this.servicePrestationsDataSource = new MatTableDataSource<null>();

    if (Array.isArray(data)) {
      data.forEach(item => {

        if (item.categorie === 'Debours') {
          const element: LigneFacturationModel =
          {
            idLignCMD: item.idLignCMD,

            montantHT: item.montantHT,

            montantTVA: item.montantTVA,

            prixTotal: item.prixTotal,
            tauxTVA: item.tauxTVA,

            tvaExterne: item.tvaExterne,
            prixUnitaire: item.prixUnitaire,
            categorie: item.categorie,
            idDossier: this.idDossier,
            libelleService: item.libelleService,
            idService: (item.service) ? item.service.idService : item.idService
          };
          this.listeLigne.push(element);
          this.listeLigneDebours.push(element);
          this.serviceDeboursDataSource = new MatTableDataSource<LigneFacturationModel>(this.listeLigneDebours);
          this.serviceDeboursDataSource.paginator = this.deboursPaginator;
          this.cdRef.detectChanges();
        }
        else {
          const element: LigneFacturationModel =
          {
            idLignCMD: item.idLignCMD,

            montantHT: item.montantHT,

            montantTVA: item.montantTVA,

            prixTotal: item.prixTotal,
            tauxTVA: item.tauxTVA,
            categorie: item.categorie,
            tvaExterne: item.tvaExterne,
            prixUnitaire: item.prixUnitaire,

            idDossier: this.idDossier,
            libelleService: item.libelleService,
            idService: (item.service) ? item.service.idService : item.idService
          };
          this.listeLigne.push(element);
          this.listeLignePrestations.push(element);
          this.servicePrestationsDataSource = new MatTableDataSource<LigneFacturationModel>(this.listeLignePrestations);
          this.servicePrestationsDataSource.paginator = this.prestationsPaginator;
          this.cdRef.detectChanges();
        }

      })

    }

  }

  findLibelleExist(lib: string, liste: LigneFacturationModel[]): boolean {
    console.log('donnee test lib  ', lib);
    const mar = liste.find(item => {
      console.log('donnee test lib  liste', item.libelleService);
      if (item.libelleService === lib) return item;
    });

    if (mar) {
      return true;
    } else {
      return false;
    }

  }

  /**
     * On form submit
     */
  onSubmitNewService(): void {
    if (this.libPrestation !== '' || this.libDebours !== '') {
      const payload = this.formatData1();

      this.createService(payload);

    }
  }
  /**
     * Format data to be saved
     */
  formatData1(): any {
    if (this.libDebours === '') {
      return {
        categorie: 'Prestations',
        libelleService: this.libPrestation,
        prixMax: this.prixPrestations

      };
    }
    else {
      return {
        categorie: 'Debours',
        libelleService: this.libDebours,
        prixMax: this.prixDebours

      };
    }

  }
  /**
  * Create the service
  * @param payload
  */
  createService(payload: any): void {
    this.isSaving = true;
    console.log('donnee creation  ', payload);
    const saveSub$ = this.dossierService.addService(payload).subscribe(
      (response) => {
        if (response) {
          this.serviceDebour = response;
          this.servicePrestation = response;
          this.isCreated = true;
          this.openSnackBar("createServiceSuccess");

          this.listeServices();
          //this.cdRef.detectChanges();

        } else {
          this.openSnackBar("createServiceError");
        }
        this.isSaving = false;
        // this.cdRef.detectChanges();
      },
      (error: HttpErrorResponse) => {
        this.isSaving = false;

        this.openSnackBar("createServiceError");
        // this.cdRef.detectChanges();
      }
    );
    this.subscriptions.push(saveSub$);
  }

  onNewDebours() {
    this.isNewDebours = true;
    this.isNewPrestation = false;
    this.libPrestation = '';
    this.cdRef.detectChanges();
  }


  onNewPrestation() {
    this.isNewPrestation = true;
    this.isNewDebours = false;
    this.libDebours = '';
    this.cdRef.detectChanges();
  }

  onAddServicePrestation() {



    if (this.libPrestation !== '') {
      if (!this.findLibelleExist(this.libPrestation, this.listeLignePrestations)) {

        const payload = this.formatData1();

        // this.onSubmitNewService();

        this.isSaving = true;
        console.log('donnee creation  ', payload);
        const saveSub$ = this.dossierService.addService(payload).subscribe(
          (response) => {
            if (response) {
              this.serviceDebour = response;
              this.servicePrestation = response;
              this.isCreated = true;
              this.openSnackBar("createServiceSuccess");

              this.ligneFacturationPrestations = {
                idLignCMD: 0,

                montantHT: this.prixPrestations,

                montantTVA: this.prixPrestations,

                prixTotal: this.prixPrestations,
                tauxTVA: this.dossier.tauxChange,

                tvaExterne: 0,
                prixUnitaire: this.prixPrestations,
                categorie: this.servicePrestation.categorie,
                idDossier: this.idDossier,
                libelleService: this.servicePrestation.libelleService,
                idService: this.servicePrestation.idService
              };
              this.listeLignePrestations.push(this.ligneFacturationPrestations);
              this.listeNewLigneDebours.push(this.ligneFacturationPrestations);
              this.servicePrestationsDataSource = new MatTableDataSource<LigneFacturationModel>(this.listeLignePrestations);
              this.servicePrestationsDataSource.paginator = this.prestationsPaginator;
              this.ligneFacturationPrestations = null;
              this.libPrestation = '';
              this.libDebours = '';
              this.prixPrestations = null;
              this.cdRef.detectChanges();


            } else {
              this.openSnackBar("createServiceError");
            }
            this.isSaving = false;
            // this.cdRef.detectChanges();
          },
          (error: HttpErrorResponse) => {
            this.isSaving = false;

            this.openSnackBar("createServiceError");
            // this.cdRef.detectChanges();
          }
        );
        this.subscriptions.push(saveSub$);
      } else {
        this.openSnackBar("serviceExist");

      }
    }
    else {
      if (!this.findLibelleExist(this.servicePrestation.libelleService, this.listeLignePrestations)) {
        this.ligneFacturationPrestations = {
          idLignCMD: 0,

          montantHT: this.prixPrestations,

          montantTVA: this.prixPrestations,

          prixTotal: this.prixPrestations,
          tauxTVA: this.dossier.tauxChange,

          tvaExterne: 0,
          prixUnitaire: this.prixPrestations,
          categorie: this.servicePrestation.categorie,
          idDossier: this.idDossier,
          libelleService: this.servicePrestation.libelleService,
          idService: this.servicePrestation.idService
        };
        this.listeLignePrestations.push(this.ligneFacturationPrestations);
        this.listeNewLigneDebours.push(this.ligneFacturationPrestations);
        this.servicePrestationsDataSource = new MatTableDataSource<LigneFacturationModel>(this.listeLignePrestations);
        this.servicePrestationsDataSource.paginator = this.prestationsPaginator;
        this.ligneFacturationPrestations = null;
        this.libPrestation = '';
        this.libDebours = '';
        this.prixPrestations = null;
        this.servicePrestation = null;
        this.cdRef.detectChanges();
      } else {
        this.openSnackBar("serviceExist");

      }
    }


  }

  onAddServiceDebours() {

    if (this.libDebours !== '') {
      if (!this.findLibelleExist(this.libDebours, this.listeLigneDebours)) {
        //this.onSubmitNewService();
        const payload = this.formatData1();

        // this.onSubmitNewService();

        this.isSaving = true;
        console.log('donnee creation  ', payload);
        const saveSub$ = this.dossierService.addService(payload).subscribe(
          (response) => {
            if (response) {
              this.serviceDebour = response;
              this.servicePrestation = response;
              this.isCreated = true;
              this.openSnackBar("createServiceSuccess");
              this.ligneFacturationDebours = {
                idLignCMD: 0,

                montantHT: this.prixDebours,
                dateLivCMD: new Date,
                montantTVA: this.prixDebours,

                prixTotal: this.prixDebours,
                tauxTVA: this.dossier.tauxChange,
                libelleService: this.serviceDebour.libelleService,
                tvaExterne: 0,
                prixUnitaire: this.prixDebours,
                categorie: this.serviceDebour.categorie,
                idDossier: this.idDossier,

                idService: this.serviceDebour.idService
              };
              this.listeLigneDebours.push(this.ligneFacturationDebours);
              this.listeNewLigneDebours.push(this.ligneFacturationDebours);
              this.serviceDeboursDataSource = new MatTableDataSource<LigneFacturationModel>(this.listeLigneDebours);
              this.serviceDeboursDataSource.paginator = this.deboursPaginator;
              this.ligneFacturationDebours = null;
              this.libPrestation = '';
              this.libDebours = '';
              this.prixDebours = null;
              this.cdRef.detectChanges();

            } else {
              this.openSnackBar("createServiceError");
            }
            this.isSaving = false;
            // this.cdRef.detectChanges();
          },
          (error: HttpErrorResponse) => {
            this.isSaving = false;

            this.openSnackBar("createServiceError");
            // this.cdRef.detectChanges();
          }
        );
        this.subscriptions.push(saveSub$);
      } else {
        this.openSnackBar("serviceExist");

      }

    } else {
      if (!this.findLibelleExist(this.serviceDebour.libelleService, this.listeLigneDebours)) {
        this.ligneFacturationDebours = {
          idLignCMD: 0,

          montantHT: this.prixDebours,
          dateLivCMD: new Date,
          montantTVA: this.prixDebours,
          categorie: this.serviceDebour.categorie,
          prixTotal: this.prixDebours,
          tauxTVA: this.dossier.tauxChange,
          libelleService: this.serviceDebour.libelleService,
          tvaExterne: 0,
          prixUnitaire: this.prixDebours,

          idDossier: this.idDossier,

          idService: this.serviceDebour.idService
        };
        this.listeLigneDebours.push(this.ligneFacturationDebours);
        this.listeNewLigneDebours.push(this.ligneFacturationDebours);
        this.serviceDeboursDataSource = new MatTableDataSource<LigneFacturationModel>(this.listeLigneDebours);
        this.serviceDeboursDataSource.paginator = this.deboursPaginator;
        this.ligneFacturationDebours = null;
        this.libPrestation = '';
        this.libDebours = '';
        this.prixDebours = null;
        this.serviceDebour = null;
        this.cdRef.detectChanges();
      } else {
        this.openSnackBar("serviceExist");

      }

    }



  }


  /**
   * Format data to be saved
   */
  formatData(): Array<LigneFacturationModel> {
    let listeLigne: Array<LigneFacturationModel> = [];
    listeLigne = [].concat(this.listeLigneDebours, this.listeLignePrestations);
    return listeLigne;
  }

  /**
   * On form submit
   */
  onSubmit(): void {

    const payload = this.formatData();

    const saveSub$ = this.dossierService.addServiceOnDossier(payload).subscribe(
      (response) => {
        if (response) {
          console.log(response);
          this.isCreated = true;
          this.openSnackBar("createServiceSuccess");

          this.listeServices();
          this.cdRef.detectChanges();
          //this.enterpriseData = new EnterpriseModel().deserialize(payload);
        } else {
          this.openSnackBar("createServiceError");
        }
        this.isSaving = false;
        // this.cdRef.detectChanges();
      },
      (error: HttpErrorResponse) => {
        this.isSaving = false;
        console.log('erreur creation  ', error);
        this.openSnackBar("createServiceError");
        // this.cdRef.detectChanges();
      }
    );
    this.subscriptions.push(saveSub$);

  }

  listeServices(): void {

    const profileSub$ = this.dossierService.getAllServices().subscribe(response => {
      // if (response.status === 200 && response.data) {
      console.log('Data', response);
      this.onGetListeServices(response);

    }, (error: HttpErrorResponse) => {

    });
    this.subscriptions.push(profileSub$);


  }

  onGetListeServices(data: any) {

    if (Array.isArray(data)) {
      data.forEach(item => {
        if (item.categorie === 'Debours') {
          const element: ServiceModel =
          {
            idService: item.idService,
            categorie: item.categorie,
            libelleService: item.libelleService,
            prixMax: item.prixMax
          };
          this.listServicesDebours.push(element);
        }
        else {
          const element: ServiceModel =
          {
            idService: item.idService,
            categorie: item.categorie,
            libelleService: item.libelleService,
            prixMax: item.prixMax
          };
          this.listServicesPrestations.push(element);
        }

      })

    }

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.serviceDeboursDataSource.filter = filterValue.trim().toLowerCase();
  }
  applyFilter1(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.servicePrestationsDataSource.filter = filterValue.trim().toLowerCase();
  }


  getDossier(): void {
    this.idDossier = this.route.snapshot.params["idDossier"];
    const profileSub$ = this.dossierService
      .getOneDossier(this.idDossier)
      .subscribe(
        (response) => {
          // if (response.status === 200 && response.data) {
          console.log("Data", response);
          this.onGetDossier(response);
          // } else {
          //   this.isInitializing = false;
          //   this.errorInitializing = true;
          // }
        },
        (error: HttpErrorResponse) => { }
      );
    this.subscriptions.push(profileSub$);
  }

  onGetDossier(item: any) {
    this.dossier = {
      clientele: item.clientele,
      cde: item.cde,
      valFob: item.valFob,
      tauxChange: item.tauxChange,
      poids: item.poids,
      ot: item.ot,

      lta: item.lta,
      valFobCfa: item.valFobCfa,
      dossier: item.dossier,
      nomDeclarant: item.nomDeclarant,
      statut: item.statut,
      da: item.da,

      dateBC: item.dateBC,

      cne: item.cne,
      numFactureFournisseur: item.numFactureFournisseur,
      declarationImportation: item.declarationImportation,
      navireVol: item.navireVol,
      numFactureFret: item.numFactureFret,
      nbrColis: item.nbrColis,
      numAssurance: item.numAssurance,
      numCertificatSgs: item.numCertificatSgs,
      certificatOrigine: item.certificatOrigine,
      certificatDispense: item.certificatDispense,
      certificatPhytosanitaire: item.certificatPhytosanitaire,
      adresseLivraison: item.adresseLivraison,
      autreDocuments: item.autreDocuments,
    };

  }

  /**
    * Show snack bar notification based on scenarios
    */
  openSnackBar(section: string): void {
    let notifMsg: string, notifBtn: string;
    switch (section) {
      case "createServiceError":
        notifMsg = "ERREUR CREATION";
        notifBtn = "UNE ERREUR S'EST PRODUITE LORS DE L'ENREGISTREMENT";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "error");
        break;
      case "createServiceSuccess":
        notifMsg = "SUCCES CREATION ";
        notifBtn = " ENREGISTREMENT EFFECTUÉ AVEC SUCCES";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "success");
        break;
      case "updateServiceError":
        notifMsg = "ERREUR CREATION";
        notifBtn = "UNE ERREUR S'EST PRODUITE LORS DE LA MISE A JOUR DU SERVICE";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "error");
        break;
      case "updateServiceSuccess":
        notifMsg = "SUCCES CREATION ";
        notifBtn = "SERVICE MISE A JOUR AVEC SUCCES";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "success");
        break;
      case "deleteServiceError":
        notifMsg = "ERREUR SUPRESSION";
        notifBtn = "UNE ERREUR S'EST PRODUITE LORS DE LA SUPRESSION DU SERVICE";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "error");
        break;
      case "deleteServiceSuccess":
        notifMsg = "SUCCES SUPRESSION ";
        notifBtn = "SERVICE SUPRIMME";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "success");
        break;
      case "serviceExist":
        notifMsg = "CE SERVICE EST DEJA ENREGISTRÉ";
        notifBtn = "VEUILLEZ VERIFIER";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "success");
        break;
      default:
        break;
    }
  }

}
