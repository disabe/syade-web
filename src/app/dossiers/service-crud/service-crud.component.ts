import { SelectionModel } from '@angular/cdk/collections';
import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ConfirmDialogModel, DeleteComponent } from '../../clientelle/delete/delete.component';
import { NotificationService } from '../../config/notification.service';
import { ServiceModel } from '../dossier';
import { DossiersApiService } from '../dossiers-api.service';

@Component({
  selector: 'app-service-crud',
  templateUrl: './service-crud.component.html',
  styleUrls: ['./service-crud.component.css']
})
export class ServiceCrudComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private dossierService: DossiersApiService,
    private notificationService: NotificationService,
    private cdRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.buildServiceForm();
    this.listeServices();
  }

  @ViewChild("myserviceForm") myserviceForm: NgForm;

  public isSaving: boolean = false;
  public isEditing: boolean = false;
  public serviceForm: FormGroup;
  public isCreated: boolean = false;
  private subscriptions: Array<Subscription> = [];
  service: ServiceModel = null;
  serviceDataSource = new MatTableDataSource<ServiceModel>(null);
  serviceSelection = new SelectionModel<ServiceModel>(true, []);
  public listServices: Array<ServiceModel> = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['categorie', 'libelle', 'cout', 'action'];
  isCreate: boolean = false;
  listeServices(): void {

    const profileSub$ = this.dossierService.getAllServices().subscribe(response => {
      // if (response.status === 200 && response.data) {
      console.log('Data', response);
      this.onGetListeServices(response);

    }, (error: HttpErrorResponse) => {

    });
    this.subscriptions.push(profileSub$);


  }

  onCreateService() {
    this.isCreate = true;
    this.isEditing = false;
    this.service = null;
    this.buildServiceForm();
    this.cdRef.detectChanges();
  }
  onUpdateService(event: any) {
    this.isEditing = true;
    this.isCreate = false;
    this.service = event;
    this.buildServiceForm();
    this.cdRef.detectChanges();
  }

  onGetListeServices(data: any) {
    if (Array.isArray(data)) {
      this.listServices = data.map(item => {
        const element: ServiceModel =
        {
          idService: item.idService,
          categorie: item.categorie,
          libelleService: item.libelleService,
          prixMax: item.prixMax
        };
        return element;
      });

    }
    this.serviceDataSource = new MatTableDataSource<ServiceModel>(this.listServices);
    this.serviceDataSource.paginator = this.paginator;

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.serviceDataSource.filter = filterValue.trim().toLowerCase();
  }

  confirmDialog(event): void {
    const message =
      "Attention action irreverssible êtes-vous certain de vouloir poursuivre?";

    const dialogData = new ConfirmDialogModel(
      "Confirmation de suppression",
      message
    );

    const dialogRef = this.dialog.open(DeleteComponent, {
      maxWidth: "400px",
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      let result: boolean;
      result = dialogResult;
      if (result) {
        const libSub$ = this.dossierService
          .deleteService(event.idService)
          .subscribe(
            (response) => {
              // if (response) {
              this.openSnackBar("deleteServiceSuccess");
              this.listeServices();
              this.cdRef.detectChanges();
              // }
            },
            (error: HttpErrorResponse) => {
              this.openSnackBar("deleteServiceError");
            }
          );
        this.subscriptions.push(libSub$);
        this.cdRef.detectChanges();
      }
    });
  }

  /**
    * Create enterprise form
    */
  buildServiceForm(): void {
    if (!this.isEditing) {
      this.serviceForm = this.fb.group({

        categorie: new FormControl({ value: '', disabled: false }, [
          Validators.required,

        ]),

        libelleService: new FormControl({ value: "", disabled: false }, [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(255),
        ]),

        prixMax: new FormControl({ value: 1, disabled: false }, [
          Validators.required,
          Validators.pattern("^[0-9]\\d*(\\.\\d{1,3})?$"),
        ])


      });
    }
    else {
      this.serviceForm = this.fb.group({

        categorie: new FormControl({ value: this.service.categorie, disabled: false }, [
          Validators.required,

        ]),

        libelleService: new FormControl({ value: this.service.libelleService, disabled: false }, [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(255),
        ]),

        prixMax: new FormControl({ value: this.service.prixMax, disabled: false }, [
          Validators.required,
          Validators.pattern("^[0-9]\\d*(\\.\\d{1,3})?$"),
        ])


      });
    }

  }

  /**
   * On form submit
   */
  onSubmit(): void {
    if (this.serviceForm.valid) {
      const payload = this.formatData();
      if (!this.isEditing) {
        this.createService(payload);

      } else {
        this.updateService(payload, this.service.idService);

      }

    }
  }

  /**
   * Create the service
   * @param payload
   */
  createService(payload: any): void {
    this.isSaving = true;
    console.log('donnee creation  ', payload);
    const saveSub$ = this.dossierService.addService(payload).subscribe(
      (response) => {
        if (response) {
          this.isCreated = true;
          this.openSnackBar("createServiceSuccess");
          this.serviceForm.reset();
          this.listeServices();
          this.cdRef.detectChanges();
          //this.enterpriseData = new EnterpriseModel().deserialize(payload);
        } else {
          this.openSnackBar("createServiceError");
        }
        this.isSaving = false;
        // this.cdRef.detectChanges();
      },
      (error: HttpErrorResponse) => {
        this.isSaving = false;
        console.log('erreur creation  ', error);
        this.openSnackBar("createServiceError");
        // this.cdRef.detectChanges();
      }
    );
    this.subscriptions.push(saveSub$);
  }
  /**
   * Create the service
   * @param payload
   */
  updateService(payload: any, id: number): void {
    this.isSaving = true;

    const saveSub$ = this.dossierService.updateService(id, payload).subscribe(
      (response) => {
        if (response) {
          this.isCreated = true;
          this.openSnackBar("updateServiceSuccess");
          this.serviceForm.reset();
          this.listeServices();
          this.cdRef.detectChanges();
          //this.enterpriseData = new EnterpriseModel().deserialize(payload);
        } else {
          this.openSnackBar("updateServiceError");
        }
        this.isSaving = false;
        // this.cdRef.detectChanges();
      },
      (error: HttpErrorResponse) => {
        this.isSaving = false;

        this.openSnackBar("updateServiceError");
        // this.cdRef.detectChanges();
      }
    );
    this.subscriptions.push(saveSub$);
  }

  /**
   * Format data to be saved
   */
  formatData(): any {
    return {
      categorie: this.serviceForm.get("categorie").value,
      libelleService: this.serviceForm.get("libelleService").value,
      prixMax: this.serviceForm.get("prixMax").value

    };
  }

  /**
   * Show snack bar notification based on scenarios
   */
  openSnackBar(section: string): void {
    let notifMsg: string, notifBtn: string;
    switch (section) {
      case "createServiceError":
        notifMsg = "ERREUR CREATION";
        notifBtn = "UNE ERREUR S'EST PRODUITE LORS DE LA CREATION DU SERVICE";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "error");
        break;
      case "createServiceSuccess":
        notifMsg = "SUCCES CREATION ";
        notifBtn = "SERVICE ENREGISTRE AVEC SUCCES";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "success");
        break;
      case "updateServiceError":
        notifMsg = "ERREUR CREATION";
        notifBtn = "UNE ERREUR S'EST PRODUITE LORS DE LA MISE A JOUR DU SERVICE";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "error");
        break;
      case "updateServiceSuccess":
        notifMsg = "SUCCES CREATION ";
        notifBtn = "SERVICE MISE A JOUR AVEC SUCCES";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "success");
        break;
      case "deleteServiceError":
        notifMsg = "ERREUR SUPRESSION";
        notifBtn = "UNE ERREUR S'EST PRODUITE LORS DE LA SUPRESSION DU SERVICE";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "error");
        break;
      case "deleteServiceSuccess":
        notifMsg = "SUCCES SUPRESSION ";
        notifBtn = "SERVICE SUPRIMME";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "success");
        break;
      default:
        break;
    }
  }

  /**
   * Fired on destruction
   */
  ngOnDestroy(): void {
    this.unsubscribe(this.subscriptions);
  }

  /**
   * Unsubscriber
   * @param subscriptions Subscription Array or subscription object. Array<Subscription> | Subscription
   */
  unsubscribe(subscriptions: Array<Subscription> | Subscription): void {
    try {
      if (Array.isArray(subscriptions)) {
        subscriptions.forEach((item) => {
          item.unsubscribe();
        });
      } else {
        subscriptions.unsubscribe();
      }
    } catch (error) { }
  }











}
