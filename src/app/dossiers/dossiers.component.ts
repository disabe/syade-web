import { SelectionModel } from '@angular/cdk/collections';
import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, Subscription } from 'rxjs';
import { ClienteleModel } from '../clientelle/clientele';
import { ClienteleApiService } from '../clientelle/clientele-api.service';
import { NotificationService } from '../config/notification.service';
import { DossierModel } from './dossier';
import { DossiersApiService } from './dossiers-api.service';
//import { ConfirmDialogModel, DeleteComponent } from './delete/delete.component';

@Component({
  selector: 'app-dossiers',
  templateUrl: './dossiers.component.html',
  styleUrls: ['./dossiers.component.css']
})
export class DossiersComponent implements OnInit {

  constructor(private dossierService: DossiersApiService,
    private clientService: ClienteleApiService,
    private cdRef: ChangeDetectorRef,
    private notificationService: NotificationService,
    public dialog: MatDialog,
    //public dialog: MatDialog,
    // private router: Router,
  ) { this.listeClients(); }


  ngOnInit(): void {

    this.listeDossiers();
    // this.userService.getAllClient().subscribe(data => this.listeClients = data.data);
    // this.listeClients.forEach(cl => console.log('ici clients  ', cl));
  }

  private subscriptions: Array<Subscription> = [];
  private activationCode: string;
  public listDossiers: Array<DossierModel> = [];
  public listClients: Array<ClienteleModel> = [];
  public isInitializing: boolean;
  public errorInitializing: boolean;
  dossierDataSource = new MatTableDataSource<DossierModel>(null);
  dossierSelection = new SelectionModel<DossierModel>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['cde', 'cne', 'da', 'lta', 'ot', 'statut', 'dossier', 'action'];
  //AttribuerDossier
  selectedClient: ClienteleModel;
  idClient: number;
  isUpdate: boolean = false;
  isEditable: boolean = false;

  applyFilter2(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dossierDataSource.filter = filterValue.trim().toLowerCase();
  }

  listeClients(): void {
    const profileSub$ = this.clientService.getAllClient().subscribe(response => {
      // if (response.status === 200 && response.data) {
      console.log('Data', response);
      this.onGetListeClients(response);
      // } else {
      //   this.isInitializing = false;
      //   this.errorInitializing = true;
      // }

    }, (error: HttpErrorResponse) => {
      this.isInitializing = false;
      this.errorInitializing = true;

    });
    this.subscriptions.push(profileSub$);
  }

  onGetListeClients(data: any) {
    if (Array.isArray(data)) {
      this.listClients = data.map(item => {
        const element: ClienteleModel =
        {
          idClient: item.idClient,
          adresseClient: item.adresseClient,
          bp: item.bp,
          dateEnreg: item.dateEnreg,
          emailClient: item.emailClient,
          faxClient: item.faxClient,
          matriculeClient: item.matriculeClient,
          minKiloTransport: item.minKiloTransport,
          minMagasin: item.minMagasin,
          minManutention: item.minManutention,
          numeroCompteBancaire: item.numeroCompteBancaire,
          numeroContribuable: item.numeroContribuable,
          numeroRegCon: item.numeroRegCon,
          raisonSocialeClient: item.raisonSocialeClient,
          siteWebClient: item.siteWebClient,
          tauxADC: item.tauxADC,
          tauxMagasin: item.tauxMagasin,
          tauxManutention: item.tauxManutention,
          tauxTransport: item.tauxTransport,
          telClient: item.telClient,
          typeOperation: item.typeOperation,
          codeIban: item.codeIban
        };
        return element;
      });
      //this.idClient = this.listClients[0].idClient;
      console.log('facture List', this.listClients[0].idClient);
    }

  }


  applyFilter(event) {

    if (event === 'null') {
      this.isEditable = false;
      this.tousDossiers();

    } else {
      this.isEditable = true;
      this.idClient = event;
      this.listeDossiers();
    }

  }

  listeDossiers(): void {
    if (this.idClient) {
      const profileSub$ = this.dossierService.getDossiersByClient(this.idClient).subscribe(response => {
        // if (response.status === 200 && response.data) {
        console.log('Data', response);
        this.onGetListeDossiers(response);
        // } else {
        //   this.isInitializing = false;
        //   this.errorInitializing = true;
        // }
        this.cdRef.detectChanges();
      }, (error: HttpErrorResponse) => {
        this.isInitializing = false;
        this.errorInitializing = true;
        this.cdRef.detectChanges();
      });
      this.subscriptions.push(profileSub$);
    } else {
      const profileSub$ = this.dossierService.getAllDossiers().subscribe(response => {
        // if (response.status === 200 && response.data) {
        console.log('Data', response);
        this.onGetListeDossiers(response);
        // } else {
        //   this.isInitializing = false;
        //   this.errorInitializing = true;
        // }
        this.cdRef.detectChanges();
      }, (error: HttpErrorResponse) => {
        this.isInitializing = false;
        this.errorInitializing = true;
        this.cdRef.detectChanges();
      });
      this.subscriptions.push(profileSub$);
    }

  }

  tousDossiers(): void {

    const profileSub$ = this.dossierService.getAllDossiers().subscribe(response => {
      // if (response.status === 200 && response.data) {
      console.log('Data', response);
      this.onGetListeDossiers(response);
      // } else {
      //   this.isInitializing = false;
      //   this.errorInitializing = true;
      // }
      this.cdRef.detectChanges();
    }, (error: HttpErrorResponse) => {
      this.isInitializing = false;
      this.errorInitializing = true;
      this.cdRef.detectChanges();
    });
    this.subscriptions.push(profileSub$);
  }


  onGetListeDossiers(data: any) {
    if (Array.isArray(data)) {
      this.listDossiers = data.map(item => {
        const element: DossierModel =
        {
          idDossier: item.idDossier,
          cde: item.cde,
          cne: item.cne,
          da: item.da,
          dateBC: item.dateBC,
          datecloture: item.datecloture,
          statut: item.statut,
          detailsBC: item.detailsBC,
          dossier: item.dossier,
          lta: item.lta,
          montantEXO: item.montantEXO,
          montantFac: item.montantFac,
          montantHTBonC: item.montantHTBonC,
          montantIR: item.montantIR,
          montantTaxale: item.montantTaxale,
          montantTSR: item.montantTSR,
          montantTTCBonC: item.montantTTCBonC,
          montantTVA: item.montantTVA,
          netAPayerBonC: item.netAPayerBonC,
          numeroBC: item.objetBC,
          objetBC: item.idDossier,
          ot: item.ot,
          poids: item.poids,
          proformaOK: item.proformaOK,
          tauxChange: item.tauxChange,
          tauxTSR: item.tauxTSR,
          tvaBC: item.tvaBC,
          typeFacture: item.typeFacture,
          valFob: item.valFob,
          clientele: item.clientele,
          declarant: item.declarant,
        };
        return element;
      });
      // console.log('facture List', this.listAvoirs);
    }
    this.dossierDataSource = new MatTableDataSource<DossierModel>(this.listDossiers);
    this.dossierDataSource.paginator = this.paginator;
    this.isInitializing = false;
    this.errorInitializing = false;
  }

  // confirmDialog(ClientID: number): void {

  //   const message = 'Attention action irreverssible êtes-vous certain de vouloir poursuivre?';

  //   const dialogData = new ConfirmDialogModel('Confirmation de suppression', message);

  //   const dialogRef = this.dialog.open(DeleteComponent, {
  //     maxWidth: '400px',
  //     data: dialogData
  //   });

  //   dialogRef.afterClosed().subscribe(dialogResult => {
  //     let result: boolean;
  //     result = dialogResult;
  //     if (result) {
  //       const profileSub$ = this.clientService.deleteClient(ClientID).subscribe(response => {
  //         console.log('Suppression du client  ', response);
  //         this.openSnackBar('deleteClientSuccess');
  //         this.listeClients();
  //         this.cdRef.detectChanges();
  //       }, (error: HttpErrorResponse) => {
  //         this.isInitializing = false;
  //         this.errorInitializing = true;
  //         this.openSnackBar('deleteClientError');
  //         this.cdRef.detectChanges();
  //       });
  //       this.subscriptions.push(profileSub$);
  //     }
  //   });
  // }


  /**
    * Show snack bar notification based on scenarios
    */
  openSnackBar(section: string): void {
    let notifMsg: string, notifBtn: string;
    switch (section) {

      case 'deleteClientError':
        notifMsg = 'ERREUR CREATION';
        notifBtn = 'UNE ERREUR S\'EST PRODUITE LORS DE LA CREATION DU CLIENT';
        this.notificationService.openSnackBar(notifMsg, notifBtn, 'error');
        break;
      case 'deleteClientSuccess':
        notifMsg = 'SUCCES DELETE ';
        notifBtn = 'CLIENT SUPPRIME DE VOTRE LISTE';
        this.notificationService.openSnackBar(notifMsg, notifBtn, 'success');
        break;
      default:
        break;
    }
  }


}
