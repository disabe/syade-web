export interface SystemeHarmoniseModel {
  idSh?: number;
  codeMercurial?: string;
  dac?: string;
  ddi?: string;
  dds?: string;
  hsCode?: string;
  ise?: string;
  isi?: string;
  ste?: string;
  tardsc?: string;
  tcc?: string;
  tcv?: string;
  tva?: string;
  vpf?: string;
}

export interface MarchandisesModel {

  idMarchandise?: number;
  libelle?: string;
  quantite?: number;
  hsCode?: string;
  prixTotalFob?: number;
  prixTotalCip?: number;
  prixCip?: number;
  assurance?: number;
  fob?: number;
  transport?: number;
  poids?: number;
  idDossier?: number;

}
