import { SelectionModel } from "@angular/cdk/collections";
import { HttpErrorResponse } from "@angular/common/http";
import { ChangeDetectorRef, Component, OnInit, ViewChild } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  NgForm,
  Validators,
} from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable, Subscription } from "rxjs";
import { map, startWith } from "rxjs/operators";
import { NotificationService } from "../../config/notification.service";
import { DeviseModel, DossierModel } from "../dossier";
import { DossiersApiService } from "../dossiers-api.service";
import { MarchandisesModel, SystemeHarmoniseModel } from "../systeme-harmonise";
import {
  CodifierComponent,
  ConfirmDialogModel1,
} from "./codifier/codifier.component";

import {
  ConfirmDialogModel,
  DeleteComponent,
} from "../../clientelle/delete/delete.component";


@Component({
  selector: "app-visualisation",
  templateUrl: "./visualisation.component.html",
  styleUrls: ["./visualisation.component.css"],
})
export class VisualisationComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private dossierService: DossiersApiService,
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.params["idDossier"]) {
      this.idDossier = this.route.snapshot.params["idDossier"];
      /**ces montants devront etre mis a jour et provenir du dossier !!! */

      /**ces montants au dessus devront etre mis a jour et provenir du dossier !!! */
      //this.getMarchandises();
      this.getListeLibelle();
      this.getListeCode();
      this.getDossier();
      this.getMarchandises();
      this.filteredCode = this.myControl1.valueChanges.pipe(
        startWith(""),
        map((value) => this._filterCode(value))
      );
      this.filteredLib = this.myControl.valueChanges.pipe(
        startWith(""),
        map((value) => this._filterLib(value))
      );
      this.builddossierForm();
    }
    this.getDevises();
  }

  @ViewChild("mydossierForm") mydossierForm: NgForm;

  marchandisesDataSource = new MatTableDataSource<MarchandisesModel>(null);
  marchandiseSelection = new SelectionModel<MarchandisesModel>(true, []);
  public ListeMarchandisesDossier: MarchandisesModel[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = [
    "libelle",
    "fob",
    "cip",
    "qte",
    "poids",
    "total",
    "totalCip",
    "hsCode",
    "action",
  ];
  messageFob: boolean = false;
  messageQte: boolean = false;
  myControl = new FormControl();
  myControl1 = new FormControl();
  public listCode: string[] = [];
  public listLibelle: string[] = [];
  filteredCode: Observable<string[]>;
  filteredLib: Observable<string[]>;
  public code: string;
  public libelle: string;
  public selectedSystemHarmonise: SystemeHarmoniseModel;
  public listeSystemeHarmoniseModel: SystemeHarmoniseModel[] = [];
  public marchandises: MarchandisesModel;
  public ListeMarchandises: MarchandisesModel[] = [];
  public isSaving: boolean = false;
  public dossierForm: FormGroup;
  public isCreated: boolean = false;
  private subscriptions: Array<Subscription> = [];
  dossier: DossierModel = null;
  idDossier: number;
  fob: number = 0;
  qte: number = 1;
  tauxChange: number = 1;
  editFob: boolean = false;
  editQte: boolean = false;
  montantTransport: number = 0;
  transportInclus: boolean = true;
  choixFobOrCip: boolean = undefined;
  montantFob: number;
  montantCip: number;
  assurance: number = 0;
  fret: number = 0;
  coefficient_charges: number = 0;
  coefficient_assurance: number = 0;
  coefficient_fret: number = 0;
  coefficient_poids: number = 0;
  etatDossier: string = "";
  qte1: number = 0;
  totalFob1: number = 0;

  totalFob: number = 0;
  totalQte: number = 0;

  updateM: boolean = false;


  listeDevises: Array<DeviseModel> = [];
  onFobSelection(){
console.log("ici choix fob: ", this.choixFobOrCip);
this.cdRef.detectChanges();
  }

  checkMessage() {
    this.totalFob
    this.totalQte
    this.messageQte = true;
    this.messageFob = true;
    if (this.dossier?.nbrColis && this.dossier?.nbrColis === this.totalQte) {
      this.messageQte = false;
    }

    if (this.dossier.valFobCfa && this.dossier.valFobCfa === this.totalFob) {
      this.messageFob = false;
    }


  }

  getDevises() {
    const codeSub$ = this.dossierService.getAllDevises().subscribe(
      (response) => {
        this.onGetDevises(response);
      },
      (error: HttpErrorResponse) => { }
    );
    this.subscriptions.push(codeSub$);
  }

  onGetDevises(data: any[]) {
    if (Array.isArray(data)) {
      this.listeDevises = data.map((item) => {
        const element: DeviseModel = {
          libelle: item.libelle,
          idDevise: item.idDevise,
          valeurCfa: item.valeurCfa,
        };
        return element;
      });
    }
  }

  updateMarch() {
    this.updateM = true;
    this.cdRef.detectChanges();
  }

  private _filterCode(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.listCode.filter((option) =>
      option.toLowerCase().includes(filterValue)
    );
  }

  private _filterLib(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.listLibelle.filter((option) =>
      option.toLowerCase().includes(filterValue)
    );
  }

  getMarchandises() {
    const codeSub$ = this.dossierService
      .getMarchandiseDossier(this.idDossier)
      .subscribe(
        (response) => {
          this.onGetMarchandise(response);
        },
        (error: HttpErrorResponse) => { }
      );
    this.subscriptions.push(codeSub$);
  }


  getTotalFob() {
    this.totalFob = 0;
    this.ListeMarchandisesDossier.forEach(item => {
      this.totalFob = this.totalFob + item.prixTotalFob;
    });
  }

  getNbrsColis() {
    this.totalQte = 0;
    this.ListeMarchandisesDossier.forEach(item => {
      this.totalQte = this.totalQte + item.quantite;
    });
  }

  onGetMarchandise(data: any[]) {
    if (Array.isArray(data)) {
      this.ListeMarchandisesDossier = data.map((item) => {
        const element: MarchandisesModel = {
          idMarchandise: item.idMarchandise,
          libelle: item.libelle,
          prixTotalFob: item.prixTotalFob,
          prixTotalCip: item.prixTotalCip,
          quantite: item.quantite,
          hsCode: item.hsCode,
          prixCip: item.prixCip,
          transport: item.transport,
          poids: item.poids,
          fob: item.fob,
          assurance: item.assurance,
          idDossier: item.idDossier
        };
        return element;
      });
      this.getTotalFob();
      this.getNbrsColis();
      this.checkMessage();
      this.marchandisesDataSource = new MatTableDataSource<DossierModel>(
        this.ListeMarchandisesDossier
      );
      this.marchandisesDataSource.paginator = this.paginator;
    }
  }

  getListeCode() {
    const codeSub$ = this.dossierService.getAllCode().subscribe(
      (response) => {
        this.listCode = response;
      },
      (error: HttpErrorResponse) => { }
    );
    this.subscriptions.push(codeSub$);
  }

  getListeLibelle() {
    const libSub$ = this.dossierService.getAllLibelle().subscribe(
      (response) => {
        this.listLibelle = response;
      },
      (error: HttpErrorResponse) => { }
    );
    this.subscriptions.push(libSub$);
  }

  findSHByCode() {
    console.log("ici code ", this.code);
    if (this.myControl1.value) {
      const codeSub$ = this.dossierService
        .getSHBYCode(this.myControl1.value)
        .subscribe(
          (response) => {
            this.selectedSystemHarmonise = response;
            const ListeMarchandises2: Array<MarchandisesModel> = [];
            console.log("ici sh ", this.selectedSystemHarmonise);
            response.forEach((item) => {
              this.marchandises = {
                libelle: item.tardsc,

                quantite: this.qte,

                hsCode: item.hsCode,

                fob: this.fob,

                idDossier: this.idDossier,
              };
              ListeMarchandises2.push(this.marchandises);
              this.ListeMarchandises.push(this.marchandises);

            });
            if (!this.findCodeExist(this.myControl1.value, this.ListeMarchandisesDossier) && !this.findCodeExist(this.myControl1.value, this.ListeMarchandises)) {
              this.marchandises = ListeMarchandises2[0];
              this.confirmDialog(this.marchandises);
            } else {
              this.openSnackBar('codeExist');
              this.ListeMarchandises = [];
            }

          },
          (error: HttpErrorResponse) => { }
        );

      this.subscriptions.push(codeSub$);
    }
  }

  findCodeExist(code: string, liste: MarchandisesModel[]): boolean {

    const mar = liste.find(item => {
      if (item.hsCode === code) return item;
    });
    if (mar) {

      this.myControl1.setValue("");
      return true;

    } else {

      this.myControl1.setValue("");
      return false;
    }

  }

  findSHByLibelle() {
    if (this.myControl.value) {
      const libSub$ = this.dossierService
        .getSHBYLibelle(this.myControl.value)
        .subscribe(
          (response) => {
            const ListeMarchandises2: Array<MarchandisesModel> = [];
            response.forEach((item) => {
              this.marchandises = {
                libelle: item.tardsc,
                quantite: this.qte,
                hsCode: item.hsCode,
                fob: this.fob,
                idDossier: this.idDossier,
              };
              ListeMarchandises2.push(this.marchandises);
              this.ListeMarchandises.push(this.marchandises);
              //this.myControl.setValue("");
            });
            if (!this.findLibelleExist(this.myControl.value, this.ListeMarchandisesDossier) && !this.findLibelleExist(this.myControl.value, this.ListeMarchandises)) {
              this.marchandises = ListeMarchandises2[0];
              this.confirmDialog(this.marchandises);
            } else {
              this.openSnackBar('libelleExist');
              this.ListeMarchandises = [];
            }
          },
          (error: HttpErrorResponse) => { }
        );
      this.subscriptions.push(libSub$);
    }
  }

  findLibelleExist(lib: string, liste: MarchandisesModel[]): boolean {
    console.log("ici lib ", lib);
    console.log("ici liste ", liste);
    const mar = liste.find(item => {
      if (item.libelle.includes(lib)) return item;
    });
    if (mar) {
      this.myControl.setValue("");
      console.log("ici ", true);
      return true;
    } else {
      console.log("ici  ", false);
      this.myControl.setValue("");
      return false;
    }

  }

  updateFob(event) {
    const findDosIndex = this.ListeMarchandises.findIndex(
      (cate) => cate === event
    );
    if (findDosIndex !== -1) {
      this.marchandises = {
        idMarchandise: event.idMarchandise,
        libelle: event.libelle,

        hsCode: event.hsCode,

        fob: this.fob,

        idDossier: this.idDossier,
      };
      this.ListeMarchandises.splice(findDosIndex, 1, this.marchandises);
      console.log("ici ma 1 ", this.marchandises);
      this.editFob = !this.editFob;
      this.fob = 0;
      this.cdRef.detectChanges();
    }
  }

  gestionCharges(){
    if (this.montantTransport > 0) {
      let liste: MarchandisesModel[] = [];
      let montantFob = 0;
      let montantCip = 0;
      let qte = 0;
      let poids = 0;

      this.ListeMarchandises.forEach((item) => {
        qte += item.quantite;
      });

      this.ListeMarchandises.forEach((item) => {
        let mar: MarchandisesModel;
        let charges = this.coefficient_charges*this.fob;
        montantFob = item.fob + charges;
        poids = this.coefficient_poids * item.quantite;
        mar = {
          idMarchandise: item.idMarchandise,
          libelle: item.libelle,
          quantite: item.quantite,
          hsCode: item.hsCode,
          transport: charges,
          fob: item.fob,
          poids: poids,
          prixTotalFob: montantFob,
          idDossier: item.idDossier,
        };
        liste.push(mar);
      });
      this.ListeMarchandises = [];
      this.ListeMarchandises.push(...liste);
    }
  }

  calculTransport() {
    if (this.montantTransport > 0) {
      let liste: MarchandisesModel[] = [];
      let montant = 0;
      let qte = 0;

      this.ListeMarchandises.forEach((item) => {
        qte += item.quantite;
      });

      this.ListeMarchandises.forEach((item) => {
        let mar: MarchandisesModel;
        let trans = (this.montantTransport / qte) * item.quantite;
        montant = item.fob + (this.montantTransport / qte) * item.quantite;
        mar = {
          idMarchandise: item.idMarchandise,
          libelle: item.libelle,
          quantite: item.quantite,
          hsCode: item.hsCode,
          transport: trans,
          fob: montant,
          idDossier: item.idDossier,
        };
        liste.push(mar);
      });
      this.ListeMarchandises = [];
      this.ListeMarchandises.push(...liste);
    }
  }

  updateQte(event) {
    const findDosIndex = this.ListeMarchandises.findIndex(
      (cate) => cate === event
    );
    if (findDosIndex !== -1) {
      this.marchandises = {
        idMarchandise: event.idMarchandise,
        libelle: event.libelle,

        quantite: this.qte,

        hsCode: event.hsCode,

        idDossier: this.idDossier,
      };
      this.ListeMarchandises.splice(findDosIndex, 1, this.marchandises);
      this.editQte = !this.editQte;
      this.qte = 1;
      this.cdRef.detectChanges();
    }
  }

  OnclickFob() {
    this.editFob = true;
  }
  OnclickQte() {
    this.editQte = true;
  }

  initQteFob() {
    this.editQte = false;
    this.editFob = false;
  }

  retirerSH(event) {
    const findDosIndex = this.ListeMarchandises.findIndex(
      (cate) => cate === event
    );
    if (findDosIndex !== -1) {
      this.ListeMarchandises.splice(findDosIndex, 1);
      this.calculQteFobBase();
      this.cdRef.detectChanges();
    }
  }

  deleteMarchandise(event) {
    //console.log('ici marc ', event);
    const libSub$ = this.dossierService
      .deleteMarchandiseDossier(event.idMarchandise)
      .subscribe(
        (response) => {
          if (response.status === 200) {
            this.openSnackBar("deleteMarchandiseSuccess");
            this.getMarchandises();
          }
        },
        (error: HttpErrorResponse) => {
          this.openSnackBar("addMarchandiseError");
        }
      );
    this.subscriptions.push(libSub$);
    this.cdRef.detectChanges();
  }

  confirmDialog(event): void {
    const dialogData = new ConfirmDialogModel1(this.fob, this.qte);

    const dialogRef = this.dialog.open(CodifierComponent, {
      maxWidth: "600px",
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      let result: any;
      result = dialogResult;
      console.log("ici result: ", result);
      if (result) {
        const findDosIndex = this.ListeMarchandises.findIndex(
          (cate) => cate === event
        );
        let charges = this.coefficient_charges*result.fob;
        let montantCip = result.fob + charges;
        let montantTotalFob = result.fob * result.qte;
        let montantTotalCip = montantCip * result.qte;
        let poid = this.coefficient_poids * result.qte;

        if (findDosIndex !== -1) {
          this.marchandises = {
            idMarchandise: event.idMarchandise,
            libelle: event.libelle,
            fob: result.fob,
            prixCip: montantCip,
            prixTotalFob: montantTotalFob,
            prixTotalCip: montantTotalCip,
            poids: poid,
            assurance: this.coefficient_assurance * result.qte,
            transport: this.coefficient_fret * result.qte,
            quantite: result.qte,
            hsCode: event.hsCode,
            idDossier: this.idDossier,
          };
          //this.gestionCharges();
          console.log("ici mar--------------: ", this.marchandises);
          this.ListeMarchandises.splice(findDosIndex, 1, this.marchandises);
          this.cdRef.detectChanges();
        }
        this.calculQteFobBase();

        this.cdRef.detectChanges();
      } else {
        const findDosIndex = this.ListeMarchandises.findIndex(
          (cate) => cate === event
        );
        if (findDosIndex !== -1) {
          this.ListeMarchandises.splice(findDosIndex, 1);
          this.cdRef.detectChanges();
        }
      }
    });
  }

  confirmDialog2(mar: MarchandisesModel): void {
    console.log('ici mar ', mar);
    const dialogData = new ConfirmDialogModel1(mar.fob, mar.quantite);

    const dialogRef = this.dialog.open(CodifierComponent, {
      maxWidth: "600px",
      data: dialogData,
    });

    // const libSub$ = this.dossierService
    //           .deleteMarchandiseDossier(mar.idMarchandise)
    //           .subscribe(
    //             (response) => {
    //               if (response.status === 200) {

    //                 this.getMarchandises();
    //               }
    //             },
    //             (error: HttpErrorResponse) => {
    //               this.openSnackBar("addMarchandiseError");
    //             }
    //           );
    //         this.subscriptions.push(libSub$);

    dialogRef.afterClosed().subscribe((dialogResult) => {
      let result: any;
      result = dialogResult;
      console.log("ici result: ", result);
      if (result) {
        const findDosIndex = this.ListeMarchandisesDossier.findIndex(
          (cate) => cate === mar
        );
        let charges = this.coefficient_charges*result.fob;
        let montantCip = result.fob + charges;
        let montantTotalFob = result.fob * result.qte;
        let montantTotalCip = montantCip * result.qte;
        let poid = this.coefficient_poids * result.qte;
        console.log("ici poids: ", poid);
        if (findDosIndex !== -1) {
          this.marchandises = {
            prixCip: montantCip,
            prixTotalFob: montantTotalFob,
            prixTotalCip: montantTotalCip,
            poids: poid,
            idMarchandise: mar.idMarchandise,
            libelle: mar.libelle,
            fob: result.fob,
            quantite: result.qte,
            assurance: this.coefficient_assurance * result.qte,
            transport: this.coefficient_fret * result.qte,
            hsCode: mar.hsCode,
            idDossier: this.idDossier,
          };
          this.ListeMarchandises.splice(findDosIndex, 1, this.marchandises);

          const libSub2$ = this.dossierService
            .saveMachandisesDossier(this.idDossier, this.ListeMarchandises)
            .subscribe(
              (response) => {
                if (response.status === 200) {
                  this.montantTransport = 0;
                  this.openSnackBar("addMarchandiseSuccess");
                  this.getMarchandises();
                  this.ListeMarchandises = [];
                  //this.updateM = false;
                }
              },
              (error: HttpErrorResponse) => {
                this.openSnackBar("addMarchandiseError");
              }
            );
          this.subscriptions.push(libSub2$);

          this.cdRef.detectChanges();
        }

      } else {

        this.cdRef.detectChanges();

      }
    });
    this.cdRef.detectChanges();
  }

  calculQteFobBase() {
    this.qte1 = 0;
    this.totalFob1 = 0;
    this.ListeMarchandises.forEach(item => {
      this.qte1 = item.quantite + this.qte1;
      this.totalFob1 = this.totalFob1 + (item.fob * item.quantite);
    })
  }


  confirmDialog1(event): void {
    const message =
      "Attention action irreverssible êtes-vous certain de vouloir poursuivre?";

    const dialogData = new ConfirmDialogModel(
      "Confirmation de suppression",
      message
    );

    const dialogRef = this.dialog.open(DeleteComponent, {
      maxWidth: "400px",
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      let result: boolean;
      result = dialogResult;
      if (result) {
        const libSub$ = this.dossierService
          .deleteMarchandiseDossier(event.idMarchandise)
          .subscribe(
            (response) => {
              if (response.status === 200) {
                this.openSnackBar("deleteMarchandiseSuccess");
                this.getMarchandises();
              }
            },
            (error: HttpErrorResponse) => {
              this.openSnackBar("addMarchandiseError");
            }
          );
        this.subscriptions.push(libSub$);
        this.cdRef.detectChanges();
      }
    });
  }

  enregistrerMarchandise() {
    //this.calculTransport();
    const libSub$ = this.dossierService
      .saveMachandisesDossier(this.idDossier, this.ListeMarchandises)
      .subscribe(
        (response) => {
          if (response.status === 200) {
            this.montantTransport = 0;
            this.openSnackBar("addMarchandiseSuccess");
            this.getMarchandises();
            this.ListeMarchandises = [];
            this.updateM = false;
          }
        },
        (error: HttpErrorResponse) => {
          this.openSnackBar("addMarchandiseError");
        }
      );
    this.subscriptions.push(libSub$);
  }

  /**
   * Create enterprise form
   */
  builddossierForm(): void {
    this.dossierForm = this.fb.group({
      clientele: new FormControl({ value: 0 }),
      cde: new FormControl({ value: this.dossier?.cde, disabled: false }, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      valFob: new FormControl({ value: this.dossier?.valFob, disabled: false }),
      valFobCfa: new FormControl({ value: this.dossier?.valFobCfa, disabled: true }),
      montantCip: new FormControl({ value: this.dossier?.montantCip, disabled: false }),
      valCipCfa: new FormControl({ value: this.dossier?.valCipCfa, disabled: true }),
      montantAssurance: new FormControl({ value: this.dossier?.montantAssurance, disabled: false }),
      montantTransport: new FormControl({ value: this.dossier?.montantTransport, disabled: false }),
      tauxChange: new FormControl({
        value: this.dossier?.tauxChange,
        disabled: false,
      }),
      poids: new FormControl({ value: this.dossier?.poids, disabled: false }),
      ot: new FormControl({ value: this.dossier?.ot, disabled: false }, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      objetBC: new FormControl({
        value: this.dossier?.objetBC,
        disabled: false,
      }),
      lta: new FormControl({ value: this.dossier?.lta, disabled: false }, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      dossier: new FormControl(
        { value: this.dossier?.dossier, disabled: false },
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]
      ),
      statut: new FormControl({ value: this.dossier?.statut, disabled: false }),
      dateBC: new FormControl({ value: this.dossier?.dateBC, disabled: false }),
      da: new FormControl({ value: this.dossier?.da, disabled: false }, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      cne: new FormControl({ value: this.dossier?.cne, disabled: false }, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      numFactureFournisseur: new FormControl(
        { value: this.dossier?.numFactureFournisseur, disabled: false },
        [Validators.minLength(1), Validators.maxLength(255)]
      ),
      declarationImportation: new FormControl(
        { value: this.dossier?.declarationImportation, disabled: false },
        [Validators.minLength(1), Validators.maxLength(255)]
      ),
      navireVol: new FormControl(
        { value: this.dossier?.navireVol, disabled: false },
        [Validators.minLength(1), Validators.maxLength(255)]
      ),
      numFactureFret: new FormControl(
        { value: this.dossier?.numFactureFret, disabled: false },
        [Validators.minLength(1), Validators.maxLength(255)]
      ),
      nbrColis: new FormControl(
        { value: this.dossier?.nbrColis, disabled: false },
        [Validators.pattern("^[0-9]\\d*(\\.\\d{1,2})?$")]
      ),
      numAssurance: new FormControl(
        { value: this.dossier?.numAssurance, disabled: false },
        [Validators.minLength(1), Validators.maxLength(255)]
      ),
      numCertificatSgs: new FormControl(
        { value: this.dossier?.numCertificatSgs, disabled: false },
        [Validators.minLength(1), Validators.maxLength(255)]
      ),
      certificatOrigine: new FormControl(
        { value: this.dossier?.certificatOrigine, disabled: false },
        [Validators.minLength(1), Validators.maxLength(255)]
      ),
      certificatDispense: new FormControl(
        { value: this.dossier?.certificatDispense, disabled: false },
        [Validators.minLength(1), Validators.maxLength(255)]
      ),
      certificatPhytosanitaire: new FormControl(
        { value: this.dossier?.certificatPhytosanitaire, disabled: false },
        [Validators.minLength(1), Validators.maxLength(255)]
      ),
      adresseLivraison: new FormControl(
        { value: this.dossier?.adresseLivraison, disabled: false },
        [Validators.minLength(1), Validators.maxLength(255)]
      ),
      autreDocuments: new FormControl(
        { value: this.dossier?.autreDocuments, disabled: false },
        [Validators.minLength(1), Validators.maxLength(255)]
      ),
    });
  }

  /**
   * On form submit
   */
  onSubmit(): void {
    if (this.dossierForm.valid) {
      const payload = this.formatData();
      this.updateDossier(payload);
    }
  }

  /**
   * Create the client
   * @param payload
   */
  updateDossier(payload: any): void {
    this.isSaving = true;
    const saveSub$ = this.dossierService
      .updateDossier(this.idDossier, payload)
      .subscribe(
        (response) => {
          if (response) {
            this.isCreated = true;
            this.openSnackBar("updateDossierSuccess");
            this.cdRef.detectChanges();
            //this.enterpriseData = new EnterpriseModel().deserialize(payload);
          } else {
            this.openSnackBar("updateDossierError");
          }
          this.isSaving = false;
          // this.cdRef.detectChanges();
        },
        (error: HttpErrorResponse) => {
          this.isSaving = false;
          this.openSnackBar("updateDossierError");
          // this.cdRef.detectChanges();
        }
      );
    this.subscriptions.push(saveSub$);
  }
  /**
   * Show snack bar notification based on scenarios
   */
  openSnackBar(section: string): void {
    let notifMsg: string, notifBtn: string;
    switch (section) {
      case "updateDossierSuccess":
        notifMsg = "MISE A JOUR REUSSI";
        notifBtn = "LA MISE A JOUR DU DOSSIER S'EST BIEN EFFECTUEE ";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "error");
        break;
      case "codeExist":
        notifMsg = "UN COLIS AVEC CE CODE EXISTE";
        notifBtn = "VEUILLEZ METTRE À JOUR LE COLIS DANS LA LISTE";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "error");
        break;
      case "libelleExist":
        notifMsg = "UN COLIS AVEC CE LIBELLE EXISTE";
        notifBtn = "VEUILLEZ METTRE À JOUR LE COLIS DANS LA LISTE";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "error");
        break;
      case "updateDossierError":
        notifMsg = "ERREUR MISE A JOUR";
        notifBtn = "UNE ERREUR S'EST PRODUITE LORS DE LA MISE A JOUR";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "error");
        break;
      case "addMarchandiseError":
        notifMsg = "ERREUR AJOUT";
        notifBtn =
          "UNE ERREUR S'EST PRODUITE LORS DE L'ENREGISTREMENT DES MARCHANDISES";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "error");
        break;
      case "addMarchandiseSuccess":
        notifMsg = "SUCCES ENREGISTREMENT ";
        notifBtn = "ENREGISTREMENT DES MARCHANDISES EFFECTUE AVEC SUCCES";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "success");
        break;
      case "deleteMarchandiseSuccess":
        notifMsg = "SUPPRESSION EFFECTIVE ";
        notifBtn = "MARCHANDISES SUPPRIMMEE DE CE DOSSIER";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "success");
        break;

      default:
        break;
    }
  }

  /**
   * Format data to be saved
   */
  formatData(): any {
    return {
      cde: this.dossierForm.get("cde").value,
      valFob: this.dossierForm.get("valFob").value,
      montantCip: this.dossierForm.get("montantCip").value,
      montantAssurance: this.dossierForm.get("montantAssurance").value,
      montantTransport: this.dossierForm.get("montantTransport").value,
      tauxChange: this.dossierForm.get("tauxChange").value,
      poids: this.dossierForm.get("poids").value,
      ot: this.dossierForm.get("ot").value,

      lta: this.dossierForm.get("lta").value,

      dossier: this.dossierForm.get("dossier").value,

      statut: this.dossierForm.get("statut").value,
      da: this.dossierForm.get("da").value,

      dateBC: this.dossierForm.get("dateBC").value,

      cne: this.dossierForm.get("cne").value,
      numFactureFournisseur: this.dossierForm.get("numFactureFournisseur")
        .value,
      declarationImportation: this.dossierForm.get("declarationImportation")
        .value,
      navireVol: this.dossierForm.get("navireVol").value,
      numFactureFret: this.dossierForm.get("numFactureFret").value,
      nbrColis: this.dossierForm.get("nbrColis").value,
      numAssurance: this.dossierForm.get("numAssurance").value,
      numCertificatSgs: this.dossierForm.get("numCertificatSgs").value,
      certificatOrigine: this.dossierForm.get("certificatOrigine").value,
      certificatDispense: this.dossierForm.get("certificatDispense").value,
      certificatPhytosanitaire: this.dossierForm.get("certificatPhytosanitaire")
        .value,
      adresseLivraison: this.dossierForm.get("adresseLivraison").value,
      autreDocuments: this.dossierForm.get("autreDocuments").value,
    };
  }

  getDossier(): void {
    this.idDossier = this.route.snapshot.params["idDossier"];
    const profileSub$ = this.dossierService
      .getOneDossier(this.idDossier)
      .subscribe(
        (response) => {
          // if (response.status === 200 && response.data) {
          console.log("Data", response);
          this.onGetDossier(response);
          // } else {
          //   this.isInitializing = false;
          //   this.errorInitializing = true;
          // }
        },
        (error: HttpErrorResponse) => { }
      );
    this.subscriptions.push(profileSub$);
  }

  onGetDossier(item: any) {
    this.dossier = {
      clientele: item.clientele,
      cde: item.cde,
      valFob: item.valFob,
      tauxChange: item.tauxChange,
      poids: item.poids,
      ot: item.ot,
      montantCip: item.montantCip,
      montantAssurance: item.montantAssurance,
      montantTransport: item.montantTransport,
      lta: item.lta,
      valFobCfa: item.valFobCfa,
      dossier: item.dossier,
      nomDeclarant: item.nomDeclarant,
      statut: item.statut,
      da: item.da,

      dateBC: item.dateBC,

      cne: item.cne,
      numFactureFournisseur: item.numFactureFournisseur,
      declarationImportation: item.declarationImportation,
      navireVol: item.navireVol,
      numFactureFret: item.numFactureFret,
      nbrColis: item.nbrColis,
      numAssurance: item.numAssurance,
      numCertificatSgs: item.numCertificatSgs,
      certificatOrigine: item.certificatOrigine,
      certificatDispense: item.certificatDispense,
      certificatPhytosanitaire: item.certificatPhytosanitaire,
      adresseLivraison: item.adresseLivraison,
      autreDocuments: item.autreDocuments,
    };
    this.montantCip = item.montantCip;
    this.montantFob = item.valFob;
    this.assurance = item.montantAssurance;
    this.fret = item.montantTransport;
    this.etatDossier = item.statut;
    this.coefficient_assurance = item?.montantAssurance / item?.nbrColis;
    this.coefficient_fret = item?.montantTransport / item?.nbrColis;
    this.coefficient_poids = item?.poids / item?.nbrColis;
    this.coefficient_charges = ( this.assurance + this.fret ) / ( this.montantFob + this.assurance + this.fret ); //ce calcul permet de prendre en charge le montant CIP ou le FOB
    //En effet si le CIP est saisi alors assurance et fret sont à 0 et si c'est le FOB qui est  saISI ALORS assurance et fob doivent obligatoirement être saisis.
    //En conséquence on a ce coefficient qui sera 0 si c'est le CIP qui est saisi.
    console.log("ici coef poids: ", this.coefficient_poids);
    this.builddossierForm();
  }

  /**
   * Fired on destruction
   */
  ngOnDestroy(): void {
    this.unsubscribe(this.subscriptions);
  }

  /**
   * Unsubscriber
   * @param subscriptions Subscription Array or subscription object. Array<Subscription> | Subscription
   */
  unsubscribe(subscriptions: Array<Subscription> | Subscription): void {
    try {
      if (Array.isArray(subscriptions)) {
        subscriptions.forEach((item) => {
          item.unsubscribe();
        });
      } else {
        subscriptions.unsubscribe();
      }
    } catch (error) { }
  }
}
