import { Component, Inject, OnInit } from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material/dialog";
@Component({
  selector: "app-codifier",
  templateUrl: "./codifier.component.html",
  styleUrls: ["./codifier.component.css"],
})
export class CodifierComponent implements OnInit {
  fob: number;
  qte: number;

  constructor(
    public dialogRef: MatDialogRef<CodifierComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogModel1
  ) {
    // Update view with given values
    this.fob = data.fob;
    this.qte = data.qte;
  }

  ngOnInit() {
   
  }

  isValid: boolean = false;

  onChange() {
    if (this.fob > 0 || this.qte >= 1) {
      this.isValid = true;
    }
  }

  onConfirm(): void {
    // Close the dialog, return true
    this.dialogRef.close({ fob: this.fob, qte: this.qte });
  }

  onDismiss(): void {
    // Close the dialog, return false
    this.dialogRef.close(null);
  }
}

/**
 * Class to represent confirm dialog model.
 *
 * It has been kept here to keep it as part of shared component.
 */
export class ConfirmDialogModel1 {
  constructor(public fob: number, public qte: number) {}
}
