import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DossierManagementComponent } from './dossier-management.component';

describe('DossierManagementComponent', () => {
  let component: DossierManagementComponent;
  let fixture: ComponentFixture<DossierManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DossierManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DossierManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
