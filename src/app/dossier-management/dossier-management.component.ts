import { SelectionModel } from "@angular/cdk/collections";
import { HttpErrorResponse } from "@angular/common/http";
import { ChangeDetectorRef, Component, OnInit, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { Subscription } from "rxjs";
import {
  ConfirmDialogModel,
  DeleteComponent,
} from "../clientelle/delete/delete.component";
import { NotificationService } from "../config/notification.service";
import { DossierModel } from "../dossiers/dossier";
import { DossiersApiService } from "../dossiers/dossiers-api.service";
import { EmployeeModel } from "../employees/Employee-model";
import { EmployeesApiService } from "../employees/employees-api.service";

@Component({
  selector: "app-dossier-management",
  templateUrl: "./dossier-management.component.html",
  styleUrls: ["./dossier-management.component.css"],
})
export class DossierManagementComponent implements OnInit {
  constructor(
    private dossierService: DossiersApiService,
    private declarantService: EmployeesApiService,
    private cdRef: ChangeDetectorRef,
    private notificationService: NotificationService,
    public dialog: MatDialog
  ) //public dialog: MatDialog,
  // private router: Router,
  { }

  ngOnInit(): void {
    this.listeEmployes();
    this.listeDossiers();
    // this.userService.getAllClient().subscribe(data => this.listeClients = data.data);
    // this.listeClients.forEach(cl => console.log('ici clients  ', cl));
  }

  private subscriptions: Array<Subscription> = [];
  private activationCode: string;
  public listDossiers: Array<DossierModel> = [];
  public listDossiersASauver: Array<DossierModel> = [];
  public listeDossiersParDeclarant: DossierModel[] = [];
  public listDeclarant: Array<EmployeeModel> = [];
  idDeclarant: number = 0;
  public selectedDossier: DossierModel;
  public isInitializing: boolean;
  public errorInitializing: boolean;

  dossierDataSource = new MatTableDataSource<DossierModel>(null);
  dossierSelection = new SelectionModel<DossierModel>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ["cne", "da", "dossier", "action"];

  selectedDeclarant: EmployeeModel;

  applyFilter2(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dossierDataSource.filter = filterValue.trim().toLowerCase();
  }

  isUpdate: boolean = false;
  isEditable: boolean = false;
  listeDossiersDeclarant() {
    const profileSub$ = this.dossierService
      .getDossiersByDeclarant(this.idDeclarant)
      .subscribe(
        (response) => {
          // if (response.status === 200 && response.data) {

          this.onGetDossierDeclarant(response);
          // } else {
          //   this.isInitializing = false;
          //   this.errorInitializing = true;
          // }
        },
        (error: HttpErrorResponse) => {
          this.isInitializing = false;
          this.errorInitializing = true;
        }
      );
    this.subscriptions.push(profileSub$);
  }
  onGetDossierDeclarant(data: any) {
    if (Array.isArray(data)) {
      this.listDossiersASauver = data.map((item) => {
        const element: DossierModel = {
          idDossier: item.idDossier,
          cde: item.cde,
          cne: item.cne,
          da: item.da,
          dateBC: item.dateBC,
          datecloture: item.datecloture,
          statut: item.statut,
          detailsBC: item.detailsBC,
          dossier: item.dossier,
          lta: item.lta,
          montantEXO: item.montantEXO,
          montantFac: item.montantFac,
          montantHTBonC: item.montantHTBonC,
          montantIR: item.montantIR,
          montantTaxale: item.montantTaxale,
          montantTSR: item.montantTSR,
          montantTTCBonC: item.montantTTCBonC,
          montantTVA: item.montantTVA,
          netAPayerBonC: item.netAPayerBonC,
          numeroBC: item.objetBC,
          objetBC: item.idDossier,
          ot: item.ot,
          poids: item.poids,
          proformaOK: item.proformaOK,
          tauxChange: item.tauxChange,
          tauxTSR: item.tauxTSR,
          tvaBC: item.tvaBC,
          typeFacture: item.typeFacture,
          valFob: item.valFob,
          clientele: item.clientele,
          declarant: item.declarant,
        };
        return element;
      });

      // console.log('facture List', this.listAvoirs);
    }
  }

  listeEmployes(): void {
    const profileSub$ = this.declarantService.getAllEmployee().subscribe(
      (response) => {
        // if (response.status === 200 && response.data) {

        this.onGetListeClients(response);
        // } else {
        //   this.isInitializing = false;
        //   this.errorInitializing = true;
        // }
      },
      (error: HttpErrorResponse) => {
        this.isInitializing = false;
        this.errorInitializing = true;
      }
    );
    this.subscriptions.push(profileSub$);
  }

  public isSelectDeclarant: boolean = false;

  applyFilter(event) {
    this.isSelectDeclarant = false;
    this.selectedDeclarant = event;

    if (this.selectedDeclarant !== null) {
      this.isSelectDeclarant = true;
      this.idDeclarant = this.selectedDeclarant.idEmploye;
      this.listeDossiersDeclarant();
    } else {
      this.listDossiersASauver = [];
    }
  }

  addDossierDeclarant(dossier: DossierModel) {
    this.selectedDossier = dossier;
    console.log(
      "ici dossier  ",
      this.selectedDossier.cde,
      this.selectedDossier.cne,
      this.selectedDossier.da
    );
    this.listDossiersASauver.push(dossier);

    const findDosIndex = this.listDossiers.findIndex(
      (cate) => cate === dossier
    );
    if (findDosIndex !== -1) {
      this.listDossiers.splice(findDosIndex, 1);
      this.dossierDataSource = new MatTableDataSource<DossierModel>(
        this.listDossiers
      );
      this.dossierDataSource.paginator = this.paginator;
      this.openSnackBar("addDossier");
      this.cdRef.detectChanges();
    }
  }

  retirerDossier(event) {
    this.listDossiers.push(event);
    const findDosIndex = this.listDossiersASauver.findIndex(
      (cate) => cate === event
    );
    if (findDosIndex !== -1) {
      this.listDossiersASauver.splice(findDosIndex, 1);
      this.dossierDataSource = new MatTableDataSource<DossierModel>(
        this.listDossiers
      );
      this.dossierDataSource.paginator = this.paginator;
      this.cdRef.detectChanges();
    }
  }

  enregistrerDossierDec(): void {
    let listeIDDossier: Array<number> = [];
    if (this.listDossiersASauver.length > 0 && this.selectedDeclarant) {
      this.listDossiersASauver.forEach((item) => {
        listeIDDossier.push(item.idDossier);
      });
      const saveSub$ = this.dossierService
        .addDossierToDeclarant(listeIDDossier, this.selectedDeclarant.idEmploye)
        .subscribe(
          (response) => {
            if (response.status === 200) {
              this.openSnackBar("createClientSuccess");
            } else {
              this.openSnackBar("createClientError");
            }
          },
          (error: HttpErrorResponse) => {
            this.openSnackBar("createClientError");
            // this.cdRef.detectChanges();
          }
        );
      this.subscriptions.push(saveSub$);
      listeIDDossier = [];
      this.listDossiersASauver = [];
      this.selectedDeclarant = null;
    } else {
      this.openSnackBar("noData");
    }
  }

  /**
   * Show snack bar notification based on scenarios
   */
  openSnackBar(section: string): void {
    let notifMsg: string, notifBtn: string;
    switch (section) {
      case "createClientError":
        notifMsg = "ERREUR CREATION";
        notifBtn = "UNE ERREUR S'EST PRODUITE LORS DE LA MISE A JOUR";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "error");
        break;
      case "addDossier":
        notifMsg = "DOSSIER A ETE MIS A DISPOSITION DU DECLARANT";
        notifBtn = "NE PAS OUBLIER DE VALIDER L'OPERATION ";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "error");
        break;
      case "createClientSuccess":
        notifMsg = "SUCCES MISE A JOUR ";
        notifBtn = "DOSSIER MIS A JOUR";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "success");
        break;
      case "noData":
        notifMsg = "ERREUR DONNEES";
        notifBtn = "VEUILLEZ SELECTIONNER DOSSIER ET DECLARANT SVP";
        this.notificationService.openSnackBar(notifMsg, notifBtn, "success");
        break;
      default:
        break;
    }
  }

  onGetListeClients(data: any) {
    if (Array.isArray(data)) {
      this.listDeclarant = data
        .map((item) => {
          const element: EmployeeModel = {
            idEmploye: item.idEmploye,
            nomEmploye: item.nomEmploye,
            adresse: item.adresse,
            dateNaissance: item.dateNaissance,
            matricule: item.matricule,
            pourcentage: item.pourcentage,
            prenomEmploye: item.prenomEmploye,
            emailEmp: item.emailEmp,
            qualite: item.qualite,
            salaireFixe: item.salaireFixe,
            telephone: item.telephone,
          };

          return element;
        })
        .filter((it) => {
          return it.qualite === "Déclarant";
        });
      //this.idClient = this.listClients[0].idClient;
      console.log(this.listDeclarant);
    }
  }

  listeDossiers(): void {
    const profileSub$ = this.dossierService.getDossiersLibres().subscribe(
      (response) => {
        // if (response.status === 200 && response.data) {
        console.log("Data", response);
        this.onGetListeDossiers(response);
        // } else {
        //   this.isInitializing = false;
        //   this.errorInitializing = true;
        // }
      },
      (error: HttpErrorResponse) => {
        this.isInitializing = false;
        this.errorInitializing = true;
        this.cdRef.detectChanges();
      }
    );
    this.subscriptions.push(profileSub$);
  }

  tousDossiers(): void {
    const profileSub$ = this.dossierService.getAllDossiers().subscribe(
      (response) => {
        // if (response.status === 200 && response.data) {
        console.log("Data", response);
        this.onGetListeDossiers(response);
        // } else {
        //   this.isInitializing = false;
        //   this.errorInitializing = true;
        // }
        this.cdRef.detectChanges();
      },
      (error: HttpErrorResponse) => {
        this.isInitializing = false;
        this.errorInitializing = true;
        this.cdRef.detectChanges();
      }
    );
    this.subscriptions.push(profileSub$);
  }

  onGetListeDossiers(data: any) {
    if (Array.isArray(data)) {
      this.listDossiers = data.map((item) => {
        const element: DossierModel = {
          idDossier: item.idDossier,
          cde: item.cde,
          cne: item.cne,
          da: item.da,
          dateBC: item.dateBC,
          datecloture: item.datecloture,
          statut: item.statut,
          detailsBC: item.detailsBC,
          dossier: item.dossier,
          lta: item.lta,
          montantEXO: item.montantEXO,
          montantFac: item.montantFac,
          montantHTBonC: item.montantHTBonC,
          montantIR: item.montantIR,
          montantTaxale: item.montantTaxale,
          montantTSR: item.montantTSR,
          montantTTCBonC: item.montantTTCBonC,
          montantTVA: item.montantTVA,
          netAPayerBonC: item.netAPayerBonC,
          numeroBC: item.objetBC,
          objetBC: item.idDossier,
          ot: item.ot,
          poids: item.poids,
          proformaOK: item.proformaOK,
          tauxChange: item.tauxChange,
          tauxTSR: item.tauxTSR,
          tvaBC: item.tvaBC,
          typeFacture: item.typeFacture,
          valFob: item.valFob,
          clientele: item.clientele,
          declarant: item.declarant,
        };
        return element;
      });
      // console.log('facture List', this.listAvoirs);
    }
    this.dossierDataSource = new MatTableDataSource<DossierModel>(
      this.listDossiers
    );
    this.dossierDataSource.paginator = this.paginator;
    this.isInitializing = false;
    this.errorInitializing = false;
  }

  confirmDialog(event): void {
    const message = "Vous allez attribuer ce dossier poursuivre?";

    const dialogData = new ConfirmDialogModel(
      "Confirmation attribution",
      message
    );

    const dialogRef = this.dialog.open(DeleteComponent, {
      maxWidth: "400px",
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      let result: boolean;
      result = dialogResult;
      if (result) {
        this.addDossierDeclarant(event);
      }
    });
  }
}
