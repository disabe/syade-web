import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../_services/auth.service';
import { TokenStorageService } from '../../_services/token-storage.service';
import { role } from '../../config/role';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];

  constructor(private authService: AuthService, private tokenStorage: TokenStorageService) { }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
      window.location.href = '/app';
    }
  }

  onSubmit(): void {
    this.authService.login(this.form).subscribe(
      data => {

        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;
        let redirectionUrl = '/app/dashboard';
        this.roles.forEach(item => {
          switch (item) {
            case role.dossierCreer.libelle_role:
              redirectionUrl = '/app/dossiers';
              break;
            case role.clientCreer.libelle_role:
              redirectionUrl = '/app/clientelle';
              break;
            case role.declarantCreer.libelle_role:
              redirectionUrl = '/app/Employes';
              break;
            case role.dossierCodifierMarchandise.libelle_role:
              redirectionUrl = '/app/dossiers';
              break;
            default:
              redirectionUrl = '/app/dossiers';
              break;
          }
        }
        );

        setTimeout(() => {
          // alert(redirectionUrl);
          window.location.href = redirectionUrl;
        }, 500);
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    );
  }

  reloadPage(): void {
    window.location.reload();
  }
}
