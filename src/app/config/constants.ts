import { environment } from '../../environments/environment';
const loc = window.location;
const currentBaseUrl = `${loc.protocol}//${loc.hostname}:${loc.port}`;

/**
 * API settings
 */
export const api = {

  baseUrl: environment.apiUrl,

 
}
