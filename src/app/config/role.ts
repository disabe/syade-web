export const role = {
	clientSupprimer: {
		id_role: 7226,
		description: 'supprimer',
		libelle_role: 'SupprimerClient'
	},
	declarantCreer: {
		"id_role": 7227,
		"description": "creer",
		"libelle_role": "CreerDeclarant"
	},
	dossierSupprimer: {
		"id_role": 7228,
		"description": "supprimer",
		"libelle_role": "SupprimerDossier"
	},
	dossierModifier: {
		"id_role": 7229,
		"description": "modifier",
		"libelle_role": "ModifierDossier"
	},
	declarantModifier: {
		"id_role": 7230,
		"description": "modifier",
		"libelle_role": "ModifierDeclarant"
	},
	dossierCreer: {
		"id_role": 7231,
		"description": "creer",
		"libelle_role": "CreerDossier"
	},
	declarantConsulter: {
		"id_role": 7232,
		"description": "consulter",
		"libelle_role": "ConsulterDeclarant"
	},
	clientCreer: {
		"id_role": 7233,
		"description": "creer",
		"libelle_role": "CreerClient"
	},
	dossierCodifierMarchandise: {
		"id_role": 7234,
		"description": "codifier_Marchandise",
		"libelle_role": "CodifierMarchandiseDossier"
	},
	declarantSupprimer: {
		"id_role": 7235,
		"description": "supprimer",
		"libelle_role": "SupprimerDeclarant"
	},
	clientModifier: {
		"id_role": 7236,
		"description": "modifier",
		"libelle_role": "ModifierClient"
	},
	clientConsulter: {
		"id_role": 7237,
		"description": "consulter",
		"libelle_role": "ConsulterClient"
	},
	dossierAttribuer: {
		"id_role": 7238,
		"description": "attribuer",
		"libelle_role": "AttribuerDossier"
	},
	dossierConsulter: {
		"id_role": 7239,
		"description": "consulter",
		"libelle_role": "ConsulterDossier"
	}
}


