import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
// import { MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';
// import { MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private snackBar: MatSnackBar) { }

  /**
   * Create snackbar notification (If panelClass is not specified on fonction call then alertType is considered)
   * @param message
   * @param action
   * @param durationInSeconds
   * @param panelClass
   * @param hPosition
   * @param vPosition
   */
  openSnackBar(message: string,
    action: string,
    alertType: string,
    panelClass: string = 'jp-snackbar-info',
    durationInSeconds: number = 10,
    hPosition: MatSnackBarHorizontalPosition = 'right',
    vPosition: MatSnackBarVerticalPosition = 'bottom') {
    if (alertType && panelClass === 'jp-snackbar-info') {
      switch (alertType) {
        case 'success':
          panelClass = 'jp-snackbar-success';
          break;
        case 'warning':
          panelClass = 'jp-snackbar-warning';
          break;
        case 'error':
          panelClass = 'jp-snackbar-error';
          break;
        case 'info':
          panelClass = 'jp-snackbar-info';
          break;
        default:
          panelClass = '';
          break;
      }
    }
    setTimeout(() => {
      this.snackBar.open(message, action, {
        duration: durationInSeconds * 1000,
        horizontalPosition: hPosition,
        verticalPosition: vPosition,
        panelClass,
      });
    });
  }
}
