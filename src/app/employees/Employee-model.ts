export interface EmployeeModel {
  idEmploye?: number;


  adresse?: string;


  dateNaissance?: Date;


  matricule?: string;


  nomEmploye?: string;

  pourcentage?: number;


  prenomEmploye?: string;

  emailEmp?: string;
  username?: string;
  password?: string;

  qualite?: string;

  salaireFixe?: number;

  telephone?: string;
  dossiers?: [];
  roles?: [];

}
