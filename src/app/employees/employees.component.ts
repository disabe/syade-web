import { SelectionModel } from '@angular/cdk/collections';
import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, Subscription } from 'rxjs';
import { NotificationService } from '../config/notification.service';
import { EmployeesApiService } from './employees-api.service';
//import { ConfirmDialogModel, DeleteComponent } from './delete/delete.component';
import { EmployeeModel } from './Employee-model';


@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  constructor(private employeeService: EmployeesApiService, private cdRef: ChangeDetectorRef,
    private notificationService: NotificationService,
    public dialog: MatDialog,
    //public dialog: MatDialog,
    // private router: Router,
  ) { }


  ngOnInit(): void {
    this.listeEmployees();
    // this.userService.getAllClient().subscribe(data => this.listeClients = data.data);
    // this.listeClients.forEach(cl => console.log('ici clients  ', cl));
  }

  private subscriptions: Array<Subscription> = [];
  private activationCode: string;
  public listEmployees: Array<EmployeeModel> = [];
  public isInitializing: boolean;
  public errorInitializing: boolean;
  employeeDataSource = new MatTableDataSource<EmployeeModel>(null);
  employeeSelection = new SelectionModel<EmployeeModel>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['nom_prenom', 'telephone', 'email', 'adresse', 'qualite', 'action']; // 'bp', 'adresse',

  employee: EmployeeModel;
  isUpdate: boolean = false;

  listeEmployees(): void {
    const profileSub$ = this.employeeService.getAllEmployee().subscribe(response => {
      // if (response.status === 200 && response.data) {
      console.log('Data', response);
      this.onGetListeEmployees(response);
      // } else {
      //   this.isInitializing = false;
      //   this.errorInitializing = true;
      // }
      this.cdRef.detectChanges();
    }, (error: HttpErrorResponse) => {
      this.isInitializing = false;
      this.errorInitializing = true;
      this.cdRef.detectChanges();
    });
    this.subscriptions.push(profileSub$);
  }

  onGetListeEmployees(data: any) {
    if (Array.isArray(data)) {
      this.listEmployees = data.map(item => {
        const element: EmployeeModel =
        {
          idEmploye: item.idEmploye,
          adresse: item.adresse,
          dateNaissance: item.dateNaissance,
          matricule: item.matricule,
          nomEmploye: item.nomEmploye,
          emailEmp: item.emailEmp,
          pourcentage: item.pourcentage,
          prenomEmploye: item.prenomEmploye,
          qualite: item.qualite,
          salaireFixe: item.salaireFixe,
          telephone: item.telephone,

        };
        return element;
      });
      // console.log('facture List', this.listAvoirs);
    }
    this.employeeDataSource = new MatTableDataSource<EmployeeModel>(this.listEmployees);
    this.employeeDataSource.paginator = this.paginator;
    this.isInitializing = false;
    this.errorInitializing = false;
  }

  confirmDialog(ClientID: number): void {

    const message = 'Attention action irreverssible êtes-vous certain de vouloir poursuivre?';

    // const dialogData = new ConfirmDialogModel('Confirmation de suppression', message);

    // const dialogRef = this.dialog.open(DeleteComponent, {
    //   maxWidth: '400px',
    //   data: dialogData
    // });

    // dialogRef.afterClosed().subscribe(dialogResult => {
    let result: boolean;
    //  result = dialogResult;
    if (result) {
      const profileSub$ = this.employeeService.deleteEmployee(ClientID).subscribe(response => {
        console.log('Suppression du client  ', response);
        this.openSnackBar('deleteClientSuccess');
        this.listeEmployees();
        this.cdRef.detectChanges();
      }, (error: HttpErrorResponse) => {
        this.isInitializing = false;
        this.errorInitializing = true;
        this.openSnackBar('deleteClientError');
        this.cdRef.detectChanges();
      });
      this.subscriptions.push(profileSub$);
    }
    // });
  }


  /**
    * Show snack bar notification based on scenarios
    */
  openSnackBar(section: string): void {
    let notifMsg: string, notifBtn: string;
    switch (section) {

      case 'deleteClientError':
        notifMsg = 'ERREUR CREATION';
        notifBtn = 'UNE ERREUR S\'EST PRODUITE LORS DE LA CREATION DU CLIENT';
        this.notificationService.openSnackBar(notifMsg, notifBtn, 'error');
        break;
      case 'deleteClientSuccess':
        notifMsg = 'SUCCES DELETE ';
        notifBtn = 'CLIENT SUPPRIME DE VOTRE LISTE';
        this.notificationService.openSnackBar(notifMsg, notifBtn, 'success');
        break;
      default:
        break;
    }
  }

  /**
    * Fired on destruction
    */
  ngOnDestroy(): void {
    this.unsubscribe(this.subscriptions);
  }

  /**
     * Unsubscriber
     * @param subscriptions Subscription Array or subscription object. Array<Subscription> | Subscription
     */
  unsubscribe(subscriptions: Array<Subscription> | Subscription): void {
    try {
      if (Array.isArray(subscriptions)) {
        subscriptions.forEach(item => {
          item.unsubscribe();
        });
      } else {
        subscriptions.unsubscribe();
      }
    } catch (error) {
    }
  }


}
