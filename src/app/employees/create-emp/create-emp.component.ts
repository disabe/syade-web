import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { NotificationService } from '../../config/notification.service';
import { EmployeeModel } from '../Employee-model';
import { EmployeesApiService } from '../employees-api.service';


@Component({
  selector: 'app-create-emp',
  templateUrl: './create-emp.component.html',
  styleUrls: ['./create-emp.component.css']
})
export class CreateEmpComponent implements OnInit {

  constructor(private employeeService: EmployeesApiService,
    private fb: FormBuilder,
    private cdRef: ChangeDetectorRef,
    private notificationService: NotificationService) {
    this.buildForm();
  }


  ngOnInit(): void {

  }

  @ViewChild('myEmployeeForm') myEmployeeForm: NgForm;

  //public isLoading: boolean = true;
  public isSaving: boolean = false;
  public employeeForm: FormGroup;
  public isCreated: boolean = false;
  private subscriptions: Array<Subscription> = [];
  listeEmployees: Observable<EmployeeModel[]>;
  employee: EmployeeModel;
  isUpdate: boolean = false;



  /**
     * Create enterprise form
     */
  buildForm(): void {
    this.employeeForm = this.fb.group({
      adresse: new FormControl({ value: '', disabled: false }, [

        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      matricule: new FormControl({ value: '', disabled: false }, [

        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      nomEmploye: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      emailEmp: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      prenomEmploye: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      qualite: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      telephone: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),


    });
  }


  /**
   * On form submit
   */
  onSubmit(): void {
    if (this.employeeForm.valid) {
      const payload = this.formatData();
      this.createEmployee(payload);

    }
  }


  /**
   * Create the client
   * @param payload
   */
  createEmployee(payload: any): void {
    this.isSaving = true;
    const saveSub$ = this.employeeService.addEmployee(payload).subscribe(response => {
      if (response.status === 200) {
        this.isCreated = true;
        this.openSnackBar('createClientSuccess');
        this.employeeForm.reset();
        //this.enterpriseData = new EnterpriseModel().deserialize(payload);

      } else {
        this.openSnackBar('createClientError');
      }
      this.isSaving = false;
      this.cdRef.detectChanges();
    }, (error: HttpErrorResponse) => {
      this.isSaving = false;
      this.openSnackBar('createClientError');
      this.cdRef.detectChanges();
    });
    this.subscriptions.push(saveSub$);
  }

  /**
   * Format data to be saved
   */
  formatData(): any {
    return {
      adresse: this.employeeForm.get('adresse').value,
      matricule: this.employeeForm.get('matricule').value,
      nomEmploye: this.employeeForm.get('nomEmploye').value,
      emailEmp: this.employeeForm.get('emailEmp').value,
      prenomEmploye: this.employeeForm.get('prenomEmploye').value,
      qualite: this.employeeForm.get('qualite').value,
      telephone: this.employeeForm.get('telephone').value,

    };
  }


  /**
   * Show snack bar notification based on scenarios
   */
  openSnackBar(section: string): void {
    let notifMsg: string, notifBtn: string;
    switch (section) {

      case 'createClientError':
        notifMsg = 'ERREUR CREATION';
        notifBtn = 'UNE ERREUR S\'EST PRODUITE LORS DE LA CREATION DU CLIENT';
        this.notificationService.openSnackBar(notifMsg, notifBtn, 'error');
        break;
      case 'createClientSuccess':
        notifMsg = 'SUCCES CREATION ';
        notifBtn = 'CLIENT ENREGISTRE AVEC SUCCES';
        this.notificationService.openSnackBar(notifMsg, notifBtn, 'success');
        break;
      default:
        break;
    }
  }

  /**
   * Fired on destruction
   */
  ngOnDestroy(): void {
    this.unsubscribe(this.subscriptions);
  }

  /**
     * Unsubscriber
     * @param subscriptions Subscription Array or subscription object. Array<Subscription> | Subscription
     */
  unsubscribe(subscriptions: Array<Subscription> | Subscription): void {
    try {
      if (Array.isArray(subscriptions)) {
        subscriptions.forEach(item => {
          item.unsubscribe();
        });
      } else {
        subscriptions.unsubscribe();
      }
    } catch (error) {
    }
  }


}

