import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmployeeModel } from './Employee-model';

const CLIENT_API = 'http://localhost:8888/api/v1/grh';
// // const AUTH_API = 'http://localhost:8080/api/auth/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class EmployeesApiService {


  constructor(private http: HttpClient) { }

  addEmployee(employee: EmployeeModel): Observable<any> {
    console.log('ici service  ', employee);
    return this.http.post(CLIENT_API + '/addDeclarant', employee, httpOptions);
  }

  getAllEmployee(): Observable<any> {
    return this.http.get(CLIENT_API + '/all');
  }

  getAllRoleDeclarant(id: number): Observable<any> {
    return this.http.get(CLIENT_API + '/allRoleDeclarant/' + id);
  }
  getAllRole(): Observable<any> {
    return this.http.get(CLIENT_API + '/allRole');
  }
  getOneEmployee(idEmployee: number): Observable<any> {
    return this.http.get(CLIENT_API + '/declarant/' + idEmployee);
  }

  deleteEmployee(idEmployee: number): Observable<any> {
    return this.http.delete(CLIENT_API + '/delete/' + idEmployee);
  }
  updateEmployee(idEmployee: number, employee: EmployeeModel): Observable<any> {
    return this.http.put(CLIENT_API + '/update/' + idEmployee, employee);
  }




}
