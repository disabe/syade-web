import { HttpErrorResponse } from '@angular/common/http';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NotificationService } from '../../config/notification.service';
import { EmployeeModel } from '../Employee-model';
import { EmployeesApiService } from '../employees-api.service';

@Component({
  selector: 'app-update-emp',
  templateUrl: './update-emp.component.html',
  styleUrls: ['./update-emp.component.css']
})
export class UpdateEmpComponent implements OnInit {

  constructor(private employeeService: EmployeesApiService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private notificationService: NotificationService) {
    this.filteredRoles = this.roleCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: string | null) => fruit ? this._filter(fruit) : this.listeRole.slice()));
  }


  ngOnInit(): void {
    if (this.route.snapshot.params['idEmp']) {
      this.onUpdate();

    }
    this.buildForm();
    this.getRole();
    this.getRoleDeclarant();
  }

  @ViewChild('roleInput') roleInput: ElementRef<HTMLInputElement>;
  @ViewChild('myEmployeeForm') myEmployeeForm: NgForm;
  selectable = false;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  roleCtrl = new FormControl();
  filteredRoles: Observable<string[]>;
  roles: string[] = [];

  //public isLoading: boolean = true;
  public isSaving: boolean = false;
  public employeeForm: FormGroup;
  public isCreated: boolean = false;
  private subscriptions: Array<Subscription> = [];
  listeEmployees: Observable<EmployeeModel[]>;
  listeRole: string[] = [];
  listeRoleDec: string[] = [];
  employee: EmployeeModel;
  isUpdate: boolean = false;
  idEmp: number;

  private _filter(value: string): string[] {

    const filterValue = value.toLowerCase();
    return this.listeRole.filter(role => role.toLowerCase().includes(filterValue));

  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.roles.push(value);
    }

    // Clear the input value
    event.input!.value = '';

    this.roleCtrl.setValue(null);
  }

  remove(fruit: string): void {
    const index = this.roles.indexOf(fruit);

    if (index >= 0) {
      this.roles.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.roles.push(event.option.viewValue);
    this.roleInput!.nativeElement.value = '';
    this.roleCtrl.setValue(null);
  }

  getRoleDeclarant() {
    this.employeeService.getAllRoleDeclarant(this.idEmp).subscribe(
      data => {
        console.log(data);
        this.onGetRoleDec(data);

      },
      err => {
        // console.log(err);
      }
    );
  }
  onGetRoleDec(data: any): string[] {
    if (Array.isArray(data) && data.length) {
      data.forEach(item => {
        this.listeRoleDec.push(item.libelleRole)
        this.roles.push(item.libelleRole);
      });

    }
    return this.listeRoleDec;
  }

  getRole() {
    this.employeeService.getAllRole().subscribe(
      data => {
        console.log(data);
        this.onGetRole(data);

      },
      err => {
        // console.log(err);
      }
    );
  }

  onGetRole(data: any): string[] {
    if (Array.isArray(data) && data.length) {
      data.forEach(item => this.listeRole.push(item.libelleRole));
      //this.roles.push(this.listeRole[0]);
    }
    return this.listeRole;
  }

  onUpdate() {
    this.idEmp = this.route.snapshot.params['idEmp'];
    this.employeeService.getOneEmployee(this.idEmp).subscribe(
      data => {
        // console.log(data);
        this.onGetEmployee(data);

      },
      err => {
        // console.log(err);
      }
    );
  }

  onGetEmployee(item: any) {

    this.employee = {
      idEmploye: item.idEmploye,
      adresse: item.adresse,
      dateNaissance: item.dateNaissance,
      matricule: item.matricule,
      nomEmploye: item.nomEmploye,
      emailEmp: item.emailEmp,
      pourcentage: item.pourcentage,
      prenomEmploye: item.prenomEmploye,
      qualite: item.qualite,
      salaireFixe: item.salaireFixe,
      telephone: item.telephone,
      username: item?.username,
      password: item?.password
    }

    this.buildForm();
  }


  /**
     * Create form with data
     */
  buildForm(): void {
    this.employeeForm = this.fb.group({
      adresse: new FormControl({ value: this.employee?.adresse, disabled: false }, [

        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      matricule: new FormControl({ value: this.employee?.matricule, disabled: false }, [

        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      nomEmploye: new FormControl({ value: this.employee?.nomEmploye, disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      emailEmp: new FormControl({ value: this.employee?.emailEmp, disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      prenomEmploye: new FormControl({ value: this.employee?.prenomEmploye, disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      qualite: new FormControl({ value: this.employee?.qualite, disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      telephone: new FormControl({ value: this.employee?.telephone, disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      password: new FormControl({ value: this.employee?.password, disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),
      re_password: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),

      username: new FormControl({ value: this.employee?.username, disabled: false }, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ]),

    });
  }


  /**
   * On form submit
   */
  onSubmit(): void {
    if (this.employeeForm.valid && (this.employeeForm.get('password').value === this.employeeForm.get('re_password').value) && this.roles.length) {
      const payload = this.formatData();
      this.updateEmployee(payload);

    }
  }

  /**
   * Create the client
   * @param payload
   */
  updateEmployee(payload: any): void {
    this.isSaving = true;
    const saveSub$ = this.employeeService.updateEmployee(this.idEmp, payload).subscribe(response => {
      if (response.status === 200) {
        this.isCreated = true;
        this.openSnackBar('createClientSuccess');
        //this.enterpriseData = new EnterpriseModel().deserialize(payload);
      }
      else {
        this.openSnackBar('createClientError');
      }
      this.isSaving = false;
      //this.cdRef.detectChanges();
    }, (error: HttpErrorResponse) => {
      this.isSaving = false;
      this.openSnackBar('createClientError');
      // this.cdRef.detectChanges();
    });
    this.subscriptions.push(saveSub$);
  }
  /**
   * Format data to be saved
   */
  formatData(): any {
    return {
      adresse: this.employeeForm.get('adresse').value,
      matricule: this.employeeForm.get('matricule').value,
      nomEmploye: this.employeeForm.get('nomEmploye').value,
      emailEmp: this.employeeForm.get('emailEmp').value,
      prenomEmploye: this.employeeForm.get('prenomEmploye').value,
      qualite: this.employeeForm.get('qualite').value,
      telephone: this.employeeForm.get('telephone').value,
      password: this.employeeForm.get('password').value,
      username: this.employeeForm.get('username').value,
      roles: this.roles
    };
  }


  /**
   * Show snack bar notification based on scenarios
   */
  openSnackBar(section: string): void {
    let notifMsg: string, notifBtn: string;
    switch (section) {

      case 'createClientError':
        notifMsg = 'ERREUR CREATION';
        notifBtn = 'UNE ERREUR S\'EST PRODUITE LORS DE LA CREATION DU CLIENT';
        this.notificationService.openSnackBar(notifMsg, notifBtn, 'error');
        break;
      case 'createClientSuccess':
        notifMsg = 'SUCCES MISE A JOUR ';
        notifBtn = 'CLIENT MISE A JOUR AVEC SUCCES';
        this.notificationService.openSnackBar(notifMsg, notifBtn, 'success');
        break;
      default:
        break;
    }
  }

  /**
   * Fired on destruction
   */
  ngOnDestroy(): void {
    this.unsubscribe(this.subscriptions);
  }

  /**
     * Unsubscriber
     * @param subscriptions Subscription Array or subscription object. Array<Subscription> | Subscription
     */
  unsubscribe(subscriptions: Array<Subscription> | Subscription): void {
    try {
      if (Array.isArray(subscriptions)) {
        subscriptions.forEach(item => {
          item.unsubscribe();
        });
      } else {
        subscriptions.unsubscribe();
      }
    } catch (error) {
    }
  }

}
